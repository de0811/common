﻿/*++
    2018 / 12 / 18
--*/
#ifndef _IN_CD_PROCESSOR
#define _IN_CD_PROCESSOR


#undef UNICODE

#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif // !_CRT_SECURE_NO_WARNINGS


#include<windows.h>
#include <list>
#include <string>
#include "cd_log.h"
#ifndef NULL
    #ifdef __cplusplus
        #define NULL 0
    #else
        #define NULL ((void *)0)
    #endif
#endif
//#pragma comment(lib, "User32.lib")

class CD_processor{
protected:
    struct ID_parent_use_pipe{
        HANDLE h_output_read = NULL;
        HANDLE h_input_write = NULL;
    };
    struct ID_child_use_pipe{
        HANDLE h_output_write = NULL;
        HANDLE h_error_write = NULL;
        HANDLE h_input_read = NULL;
        HANDLE h_process = NULL;
    };
    struct ID_read_info{
        HANDLE h_thread = NULL;
        DWORD thread_id = NULL;
    };
    struct D_processor_info : public ID_parent_use_pipe, ID_child_use_pipe, ID_read_info{
    };
protected:
    D_processor_info _processor_info;
    std::list<std::string> _read_data;
	CRITICAL_SECTION _critical_section;

private:
    int __create_pipe(D_processor_info &processor_info);
    int __create_child_processor(ID_child_use_pipe &child_use_pipe, const char* cmd);
    int __close_child_pipe(ID_child_use_pipe &child_use_pipe);
    int __close_parent_pipe(ID_parent_use_pipe &parent_use_pipe);
    DWORD __kill_thread(ID_read_info &read_info);
    DWORD __kill_process(ID_child_use_pipe &child_use_pipe);
	std::string __get_error_code(std::string str_err);

public:
	CD_processor() {
		InitializeCriticalSection(&_critical_section);
	}
	~CD_processor() {
		DeleteCriticalSection(&_critical_section);
	}
    std::string pop_data();
    //아직 읽을 내용이 남았는지 확인(thread가 아직 종료 되지 않았다면 아직 읽을 내용은 추가로 생길 것이기에 true 반환)
    bool is_read();
    int read_output(HANDLE hPipeRead);
    friend DWORD CALLBACK read_thread(LPVOID p_processor);
    int write_input(HANDLE hPipeWrite, char* read_buff);
    int simple_write_input(char* read_buff);
    int run(const char* cmd);
    int clear();
};

std::string CD_processor::__get_error_code(std::string str_err) {
	DWORD err_num = GetLastError();
	char buff[1000] = { 0, };
	sprintf(buff, "Error !! Processor::%s, [%d]\n", str_err.c_str(), err_num);
    str_err = buff;
	return str_err;
}

DWORD CD_processor::__kill_process(ID_child_use_pipe &child_use_pipe){
    DWORD result = 0;
    //이거 기다리는거 아닌가 싶은데... 나중에 다시 찾아봐야함
    GetExitCodeProcess(child_use_pipe.h_process, &result);
    child_use_pipe.h_process = NULL;
    return result;
}

DWORD CD_processor::__kill_thread(ID_read_info &read_info){
    DWORD result = 0;
    //이거 기다리는거 아닌가 싶은데... 나중에 다시 찾아봐야함
    GetExitCodeThread(read_info.h_thread, &result);
    read_info.h_thread = NULL;
    read_info.thread_id = NULL;
    return result;
}



bool CD_processor::is_read(){
    bool check = false;
    if(_processor_info.h_thread == NULL){
        return _read_data.size() > 0;
    }
    else{
        return true;
    }
}

int CD_processor::__create_pipe(D_processor_info &processor_info){
    SECURITY_ATTRIBUTES sa;
    // Set up the security attributes struct.
    sa.nLength = sizeof(SECURITY_ATTRIBUTES);
    sa.lpSecurityDescriptor = NULL;
    sa.bInheritHandle = TRUE;

	/*oroginal
    // Create the child output pipe.
    if (!CreatePipe(&processor_info.h_output_read, &processor_info.h_output_write, &sa, 0)){
        CD_log::print(CD_log::LOG_ERROR, "%s\n", __get_error_code("CreatePipe").c_str());
        return -1;
    }

    // Create a duplicate of the output write handle for the std error
    // write handle. This is necessary in case the child application
    // closes one of its std output handles.
    if (!DuplicateHandle(GetCurrentProcess(), processor_info.h_output_write, GetCurrentProcess(), &processor_info.h_error_write, 0, TRUE, DUPLICATE_SAME_ACCESS)){
        CD_log::print(CD_log::LOG_ERROR, "%s\n", __get_error_code("DuplicateHandle").c_str());
        return -1;
    }
	*/

	//부모에서 듣는거 만들기
	HANDLE h_output_read_tmp = 0;
	// Create the child output pipe.
	if (!CreatePipe(&h_output_read_tmp, &processor_info.h_output_write, &sa, 0)) {
		CD_log::print(CD_log::LOG_ERROR, "%s\n", __get_error_code("CreatePipe").c_str());
		return -1;
	}
	
	if (!DuplicateHandle(GetCurrentProcess(), processor_info.h_output_write, GetCurrentProcess(), &processor_info.h_error_write, 0, TRUE, DUPLICATE_SAME_ACCESS)) {
		CD_log::print(CD_log::LOG_ERROR, "%s\n", __get_error_code("DuplicateHandle").c_str());
		return -1;
	}

	if (!DuplicateHandle(GetCurrentProcess(), h_output_read_tmp, GetCurrentProcess(), &processor_info.h_output_read, 0, FALSE, DUPLICATE_SAME_ACCESS)) {
		CD_log::print(CD_log::LOG_ERROR, "%s\n", __get_error_code("DuplicateHandle").c_str());
		return -1;
	}

	/*orginal
	// Create the child input pipe.
	if (!CreatePipe(&processor_info.h_input_read, &processor_info.h_input_write, &sa, 0)){
		CD_log::print(CD_log::LOG_ERROR, "%s\n", __get_error_code("CreatePipe").c_str());
		return -1;
	}
	*/

	//부모에서 말하는거 만들기
	HANDLE h_input_write_tmp = 0;

    if (!CreatePipe(&processor_info.h_input_read, &h_input_write_tmp, &sa, 0)){
        CD_log::print(CD_log::LOG_ERROR, "%s\n", __get_error_code("CreatePipe").c_str());
        return -1;
    }
	if (!DuplicateHandle(GetCurrentProcess(), h_input_write_tmp, GetCurrentProcess(), &processor_info.h_input_write, 0, FALSE, DUPLICATE_SAME_ACCESS)) {
        CD_log::print(CD_log::LOG_ERROR, "%s\n", __get_error_code("DuplicateHandle").c_str());
		return -1;
	}

	//tmp들 모두 지우기
	if (!CloseHandle(h_output_read_tmp)) {
		CD_log::print(CD_log::LOG_ERROR, "%s\n", __get_error_code("CloseHandle").c_str());
		return -1;
	}
	if (!CloseHandle(h_input_write_tmp)) {
        CD_log::print(CD_log::LOG_ERROR, "%s\n", __get_error_code("CloseHandle").c_str());
		return -1;
	}

    return 0;
}

int CD_processor::__create_child_processor(ID_child_use_pipe &child_use_pipe, const char* cmd){
    PROCESS_INFORMATION pi;
    STARTUPINFO si;

    // Set up the start up info struct.
    ZeroMemory(&si, sizeof(STARTUPINFO));
    si.cb = sizeof(STARTUPINFO);
    si.dwFlags = STARTF_USESTDHANDLES;
    si.hStdOutput = child_use_pipe.h_output_write;
    si.hStdInput = child_use_pipe.h_input_read;
    si.hStdError = child_use_pipe.h_error_write;
	//si.dwFlags = STARTF_USESHOWWINDOW;
	si.dwFlags = STARTF_USESTDHANDLES;
	//si.wShowWindow = SW_HIDE;

    /*
    if (!CreateProcess(NULL, (char*)"Child.EXE", NULL, NULL, TRUE,
        CREATE_NEW_CONSOLE, NULL, NULL, &si, &pi))
        DisplayError((char*)"CreateProcess");
    */
    if (!CreateProcess(NULL, (char*)cmd, NULL, NULL, TRUE, 0, NULL, NULL, &si, &pi)){
        CD_log::print(CD_log::LOG_ERROR, "%s\n", __get_error_code("CreateProcess").c_str());
        return -1;
    }

    // Set global child process handle to cause threads to exit.
    child_use_pipe.h_process = pi.hProcess;

    // Close any unnecessary handles.
    if (!CloseHandle(pi.hThread)){
        CD_log::print(CD_log::LOG_ERROR, "%s\n", __get_error_code("CloseHandle").c_str());
        return -1;
    }
    return 0;
}

int CD_processor::__close_child_pipe(ID_child_use_pipe &child_use_pipe){
    if(child_use_pipe.h_output_write != NULL){
        if (!CloseHandle(child_use_pipe.h_output_write)){
            CD_log::print(CD_log::LOG_ERROR, "%s\n", __get_error_code("CloseHandle").c_str());
            return -1;
        }
        child_use_pipe.h_output_write = NULL;
    }

    if(child_use_pipe.h_input_read != NULL){
        if (!CloseHandle(child_use_pipe.h_input_read)){
            CD_log::print(CD_log::LOG_ERROR, "%s\n", __get_error_code("CloseHandle").c_str());
            return -1;
        }
        child_use_pipe.h_input_read = NULL;
    }

    if( child_use_pipe.h_error_write != NULL){
        if (!CloseHandle(child_use_pipe.h_error_write)){
            CD_log::print(CD_log::LOG_ERROR, "%s\n", __get_error_code("CloseHandle").c_str());
            return -1;
        }
        child_use_pipe.h_error_write = NULL;
    }

    return 0;
}

int CD_processor::__close_parent_pipe(ID_parent_use_pipe &parent_use_pipe){
    if(parent_use_pipe.h_output_read != NULL){
        if (!CloseHandle(parent_use_pipe.h_output_read)){
            CD_log::print(CD_log::LOG_ERROR, "%s\n", __get_error_code("CloseHandle").c_str());
            return -1;
        }
        parent_use_pipe.h_output_read = NULL;
    }

    if(parent_use_pipe.h_input_write != NULL){
        if (!CloseHandle(parent_use_pipe.h_input_write)){
            CD_log::print(CD_log::LOG_ERROR, "%s\n", __get_error_code("CloseHandle").c_str());
            return -1;
        }
        parent_use_pipe.h_input_write = NULL;
    }

    return 0;
}

int CD_processor::read_output(HANDLE hPipeRead){
	CHAR lpBuffer[256] = { 0, };
    DWORD nBytesRead = 0;
    //DWORD nCharsWritten;

    if (!ReadFile(hPipeRead, lpBuffer, sizeof(lpBuffer)-1, &nBytesRead, NULL) || !nBytesRead)
    {
		if (GetLastError() == ERROR_BROKEN_PIPE) {
            return -1; // pipe done - normal exit path.
		}
        else
        {
            CD_log::print(CD_log::LOG_ERROR, "%s\n", __get_error_code("CloseHandle").c_str());
            //DisplayError((char*)"ReadFile"); // Something bad happened.
            return -1;
        }
    }
    // 여기서 데이터를 받는다
    if(nBytesRead != 0){
        std::string tmp_string(lpBuffer);
		EnterCriticalSection(&_critical_section);
		//__debug
		printf("%s", tmp_string.c_str());
		//
        _read_data.push_back(tmp_string);
		LeaveCriticalSection(&_critical_section);
    }


    return 0;
}

std::string CD_processor::pop_data(){
	if (_read_data.size() == 0) {
		return "";
	}
	EnterCriticalSection(&_critical_section);
	//std::string tmp_str;
	//for (std::list<std::string>::iterator iter = _read_data.begin(); iter != _read_data.end(); iter++) {
		//tmp_str += (*iter);
	//}
	//while (_read_data.size() != 0) {
		//tmp_str += _read_data.front();
		//_read_data.pop_front();
	//}
	std::string tmp_str = _read_data.front();
    _read_data.pop_front();
	LeaveCriticalSection(&_critical_section);
	return tmp_str;
}

static DWORD CALLBACK read_thread(LPVOID lpv_processor){
    DWORD result = 0;
	CD_processor *p_processor = (CD_processor*)lpv_processor;
    while( result == 0 ){
        result = p_processor->read_output(p_processor->_processor_info.h_output_read);
		//CD_log::print(CD_log::LOG_ERROR, "thread result = %d\n", result);
    }
    //pipe가 깨져서 나옴
    p_processor->__close_parent_pipe( p_processor->_processor_info );
    p_processor->_processor_info.h_thread = NULL;
    p_processor->_processor_info.thread_id = NULL;
    return result;
}

int CD_processor::simple_write_input(char* read_buff){
    return write_input(_processor_info.h_input_write, read_buff);
}

int CD_processor::write_input(HANDLE hPipeWrite, char* read_buff){
    // CHAR read_buff[256];
    //쓸 길이

	DWORD nBytesRead = strlen(read_buff) + 1;
	char* tmp_str = read_buff;
	if (!__str_end_with(tmp_str, (char*)"\n")) {
		tmp_str = new char[++nBytesRead];
		__strcpy_ss(tmp_str, nBytesRead, read_buff, nBytesRead - 1);
		__strcat_ss(tmp_str, nBytesRead, "\n", 1);
	}
	
    //얼마나 썻는지에 대한 길이 받아오기
    DWORD nBytesWrote;
    // Get input from our console and send it to child through the pipe.
	
    if (!WriteFile(hPipeWrite, tmp_str, nBytesRead, &nBytesWrote, NULL))
    {
        if (GetLastError() == ERROR_NO_DATA)
            return -1; // Pipe was closed (normal exit path).
        else{
            CD_log::print(CD_log::LOG_ERROR, "%s\n", __get_error_code("WriteFile").c_str());
            return -1;
        }
    }
	FlushFileBuffers(hPipeWrite);
	char log[10000];
	sprintf(log, "%s %d \n", tmp_str, nBytesWrote);
	OutputDebugString(log);
	

    return (int)nBytesWrote;
}

int CD_processor::clear(){
    if(is_read()){
        __close_child_pipe(_processor_info);
        __close_parent_pipe(_processor_info);
        __kill_thread(_processor_info);
        __kill_process(_processor_info);
        _read_data.clear();
    }
	return 0;
}

int CD_processor::run(const char* cmd){
    //thread 강제 종료까지 넣으려고 보니... 꼭 그래야하나?
    clear();
    
	if (__create_pipe(_processor_info)) {
        __close_child_pipe(_processor_info);
        __close_parent_pipe(_processor_info);
		return -1;
	}

    // Get std input handle so you can close it and force the ReadFile to
    // fail when you want the input thread to exit.
    // if ((hStdIn = GetStdHandle(STD_INPUT_HANDLE)) == INVALID_HANDLE_VALUE)
        // DisplayError((char*)"GetStdHandle");

	if (__create_child_processor(_processor_info, cmd)) {
        __close_child_pipe(_processor_info);
        __close_parent_pipe(_processor_info);
		return -1;
	}

    // Close pipe handles (do not continue to modify the parent).
    // You need to make sure that no handles to the write end of the
    // output pipe are maintained in this process or else the pipe will
    // not close when the child process exits and the ReadFile will hang.
    //넘겨준 pipe는 이제 내가 가지고 있을 필요가 없기 때문에 닫아준다 
    if( __close_child_pipe(_processor_info) ){
        __close_parent_pipe(_processor_info);
        return -1;
    }

    // Launch the thread that gets the input and sends it to the child.
    _processor_info.h_thread = CreateThread(NULL, 0, read_thread, (LPVOID)this, 0, &_processor_info.thread_id);
    if (_processor_info.h_thread == NULL){
        CD_log::print(CD_log::LOG_ERROR, "%s\n", __get_error_code("CreateThread").c_str());
        return -1;
    }

/*
    if (WaitForSingleObject(hThread, INFINITE) == WAIT_FAILED)
        DisplayError((char*)"WaitForSingleObject");

    __close_parent_pipe(processor_info);
*/

/*
	DWORD dwExitCode = 0;
	do {
		GetExitCodeThread(_processor_info.h_thread, &dwExitCode);
		for (std::list<std::string>::iterator iter = _read_data.begin(); iter != _read_data.end(); iter++) {
			CD_log::print(CD_log::LOG_ERROR, "%s\n", (*iter).c_str());
		}
		CD_log::print(CD_log::LOG_ERROR, "\n\n\n");
	} while (dwExitCode == STILL_ACTIVE);
    */
   /*
   while(is_read()){
       std::string tmp_str = pop_data();
       if(!tmp_str.empty()) CD_log::print(CD_log::LOG_ERROR, "%s\n", tmp_str.c_str());
   }
   */

    return 0;
}




#endif /*_IN_CD_PROCESSOR*/