﻿

#ifndef _IN_LIB_COMMAND
#define _IN_LIB_COMMAND

#include "c_string.h"
#include "c_file.h"

#ifdef _CPP_MODULE
#include "cd_log.h"
#include "cd_option.h"
#include "cd_processor.h"
#include "cd_xml.h"
#endif /* _CPP_MODULE*/

#ifdef _ANDROID_CONTROL_MODULE
#include "android/cd_dumpsys_window.h"
#include "android/inc_uiautomator.h"
#include "android/cd_adb_send_event.h"
#endif /* _ANDROID_COMMAND*/


#endif /*  _IN_LIB_COMMAND */