﻿/*++
    2019 / 1 / 9
--*/
#ifndef _IN_CD_DUMPSYS_WINDOW
#define _IN_CD_DUMPSYS_WINDOW


#undef UNICODE
#include "../common.h"
// #include <list>
// #include <string>
// #include "cd_log.h"
// #include "c_string.h"

#ifndef IN
#define IN  
#endif
#ifndef OUT
#define OUT  
#endif
#ifndef NULL
    #ifdef __cplusplus
        #define NULL 0
    #else
        #define NULL ((void *)0)
    #endif
#endif


class CD_dumpsys_window{
	std::string _device_id;
public:
	std::string _focused_window;
	bool b_error;
public:
	unsigned int parssing(std::string device_id);
};
unsigned int CD_dumpsys_window::parssing(std::string device_id){
	if( device_id.empty() ){
		CD_log::print(CD_log::LOG_ERROR, "cd_dumpsys_window::parssing(device = is NULL)\n");
		return -1;
	}
	_device_id = device_id;
	std::string front_adb = std::string("adb -s ") + device_id + " shell dumpsys window";

	CD_processor cd_processor;
	cd_processor.run(front_adb.c_str());
	std::string full_str;
	std::string tmp_str;
	while( cd_processor.is_read() ){
		tmp_str = cd_processor.pop_data();
		if(tmp_str.empty()){
			continue;
		}
		full_str += tmp_str;
	}

	char **pfull_str_split = NULL;
	unsigned int full_str_split_count = 0;
	__str_split((char*)full_str.c_str(), (char*)"\n", &pfull_str_split, &full_str_split_count);

	for(unsigned int i = 0; i < full_str_split_count; i++){

		if( __strstr(pfull_str_split[i], "mFocusedWindow=Window") != NULL ){
			char* move_val = __trim(pfull_str_split[i]);
			if( __strstr(move_val, "Error: ") != NULL || __strstr(move_val, "Error") != NULL){
				_focused_window = move_val;
				b_error = true;
				continue;
			}
			else{
				char ** p_focused_split = NULL;
				unsigned int focused_split_count = 0;
				__str_split(move_val, (char*)" ", &p_focused_split, &focused_split_count);
				if(focused_split_count != 3){
					CD_log::print(CD_log::LOG_ERROR, "ERROR !!! CD_dumpsys_window::parssing(%s) find mFocusedWindow is fcoused_split_count Not 3\n", device_id.c_str());
					__str_split_delete(&p_focused_split, focused_split_count);
					// __str_split_delete(&pfull_str_split, full_str_split_count);
					// return -1;
					continue;
				}
				(*__strstr(p_focused_split[2], "}")) = '\0';
				_focused_window = p_focused_split[2];

				//delete
				__str_split_delete(&p_focused_split, focused_split_count);
				break;
			}
		} //if( __strstr(pfull_str_split[i], "mFocusedWindow=Window") != NULL ){

	} //for(unsigned int i = 0; i < full_str_split_count; i++){

	//p_device_info->print();

	//delete
	__str_split_delete(&pfull_str_split, full_str_split_count);


	return 0;
}



#endif /* _IN_CD_DUMPSYS_WINDOW */
