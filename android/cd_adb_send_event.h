﻿/*++
    2019 / 2 / 8
--*/
#ifndef _IN_CD_ADB_SEND_EVENT
#define _IN_CD_ADB_SEND_EVENT


#undef UNICODE
#include "../common.h"

#include <sstream>
//#include <algorithm>
class CD_adb_send_event{
public:
	enum{
		eSTATE_NONE,
		eSTATE_START,
		eSTATE_RUN,
		eSTATE_END,
		eSTATE_CLICK,
	};
private:
	class D_adb_send_event_data{
	public:
		//name, key, min_x, min_y, init_x, init_y, max_x, max_y, order);
		std::list<int> _keys;
		int _x = 0;
		int _y = 0;
		int _pow = 1;
		unsigned int _state = 0;
		std::string _name;
		int _x_min = 0;
		int _y_min = 0;
		int _x_max = 0;
		int _y_max = 0;
		int _order = 0;
		//시작도 안했다면 -1
		unsigned int _slot = -1;

		int _x_init = 0;
		int _y_init = 0;
		int _x_before = 0;
		int _y_before = 0;

		std::list<std::string> _chains;
	public:
		// D_adb_send_event_data(){}
		D_adb_send_event_data(std::string name, std::list<int> keys, int x_min, int y_min, int x, int y, int x_max, int y_max, int order) {
			init(name, keys, x_min, y_min, x, y, x_max, y_max, order);
		}
		bool operator <(const D_adb_send_event_data &prh) {
			return this->_order < prh._order;
		}
		bool operator >(const D_adb_send_event_data &prh) {
			return this->_order > prh._order;
		}
		void init(std::string name, std::list<int> keys, int x_min, int y_min, int x, int y, int x_max, int y_max, int order) {
			this->_name = name;
			this->_state = eSTATE_NONE;
			this->_keys = keys;
			this->_x_min = x_min;
			this->_y_min = y_min;
			this->_x_init = x;
			this->_y_init = y;
			this->_x_max = x_max;
			this->_y_max = y_max;
			this->_order = order;
			set_position(x, y);
		}
		void set_position(int x, int y){
			this->_x = x;
			this->_y = y;
		}
		void set_state(unsigned int state){
			this->_state = state;
		}
	};
private:
	int _tracking_id = 0; 
	std::list<D_adb_send_event_data> _data;
	std::string _str_send_event;
	CD_processor send_processor;
	//사용해도 되는 슬롯 찾기
	unsigned int __get_empty_slot();
	//슬롯이 몇개인지 확인
	unsigned int __check_slot();
	std::string __itos(int i);
	int __touch_down(std::string name);
	int __touch_up(std::string name);
	int __touch_run(std::string name);
	int __touch_click(std::string name);
	void __key_event_handling();
	void __key_event_run();
public:
	~CD_adb_send_event(){close();}
	void init();
	void add_adb_send_event_data(std::string name, std::list<int> keys, int x_min, int y_min, int x, int y, int x_max, int y_max, int order);
	void set_state(std::string name, unsigned int state);
	unsigned int get_state(std::string name);
	int get_order(std::string name);
	void set_x_y(std::string name, int x, int y);
	void get_x_y(std::string name, int &x, int &y);
	void get_init_x_y(std::string name, int &x, int &y);
	void set_event_reset(std::string name);
	void set_chain(std::list<std::string> chains);
	void run();
	void close();
};

void CD_adb_send_event::close() {
	if( send_processor.is_read() ){
		send_processor.simple_write_input((char*)"exit");
		//Sleep(50);
		send_processor.simple_write_input((char*)"exit");
	}
}

void CD_adb_send_event::init(){
	send_processor.run("adb shell");
	Sleep(2000);
	send_processor.simple_write_input((char*)"/data/local/tmp/fastevent");
}

std::string CD_adb_send_event::__itos(int i){
	std::ostringstream s;
	s << i;
	return s.str();
}

//슬롯이 몇개인지 확인
unsigned int CD_adb_send_event::__check_slot(){
	unsigned int result = 0;
	for(std::list<D_adb_send_event_data>::iterator iter = _data.begin(); iter != _data.end(); iter++){
		if(iter->_slot >= 0){
			++result;
		}
	}
	return result;
}

//사용해도 되는 슬롯 찾기
unsigned int CD_adb_send_event::__get_empty_slot(){
	unsigned int result = 0;
	std::list<D_adb_send_event_data>::iterator iter = _data.begin();
	while(iter != _data.end()){
		if(iter->_slot == result){
			++result;
			iter = _data.begin();
			continue;
		}
		++iter;
	}
	return result;
}


void CD_adb_send_event::add_adb_send_event_data(std::string name, std::list<int> keys, int x_min, int y_min, int x, int y, int x_max, int y_max, int order) {
	_data.push_back(D_adb_send_event_data(name, keys, x_min, y_min, x, y, x_max, y_max, order));
}

void CD_adb_send_event::set_state(std::string name, unsigned int state){
	for(std::list<D_adb_send_event_data>::iterator iter = _data.begin(); iter != _data.end(); iter++){
		if(__strcmp(name.c_str(), iter->_name.c_str()) == 0){
			if (state == eSTATE_START && iter->_state == eSTATE_RUN) return;
			if (state == eSTATE_END && iter->_state == eSTATE_NONE) return;
			iter->_state = state;
			return;
		}
	}
}

unsigned int CD_adb_send_event::get_state(std::string name) {
	for (std::list<D_adb_send_event_data>::iterator iter = _data.begin(); iter != _data.end(); iter++) {
		if (__strcmp(name.c_str(), iter->_name.c_str()) == 0) {
			return iter->_state;
		}
	}
	return eSTATE_NONE;
}

int CD_adb_send_event::get_order(std::string name) {
	for (std::list<D_adb_send_event_data>::iterator iter = _data.begin(); iter != _data.end(); iter++) {
		if (__strcmp(name.c_str(), iter->_name.c_str()) == 0) {
			return iter->_order;
		}
	}
	return -1;
}

void CD_adb_send_event::set_x_y(std::string name, int x, int y) {
	for (std::list<D_adb_send_event_data>::iterator iter = _data.begin(); iter != _data.end(); iter++) {
		if (__strcmp(name.c_str(), iter->_name.c_str()) == 0) {
			iter->_x = x;
			iter->_y = y;
			return;
		}
	}
}

void CD_adb_send_event::get_x_y(std::string name, int &x, int &y) {
	for (std::list<D_adb_send_event_data>::iterator iter = _data.begin(); iter != _data.end(); iter++) {
		if (__strcmp(name.c_str(), iter->_name.c_str()) == 0) {
			x = iter->_x;
			y = iter->_y;
			return;
		}
	}
}

void CD_adb_send_event::get_init_x_y(std::string name, int &x, int &y) {
	for (std::list<D_adb_send_event_data>::iterator iter = _data.begin(); iter != _data.end(); iter++) {
		if (__strcmp(name.c_str(), iter->_name.c_str()) == 0) {
			x = iter->_x_init;
			y = iter->_y_init;
			return;
		}
	}
}

void CD_adb_send_event::set_event_reset(std::string name){
	int x = 0;
	int y = 0;
	get_init_x_y(name, x, y);
	set_x_y(name, x, y);
}

void CD_adb_send_event::set_chain(std::list<std::string> chains) {
	//sorting 위해 전체 정보 추출
	std::list<D_adb_send_event_data> tmp_sort;
	for (std::list<D_adb_send_event_data>::iterator iter = _data.begin(); iter != _data.end(); iter++) {
		//bool b_chain = false;
		for (std::list<std::string>::iterator chain = chains.begin(); chain != chains.end(); chain++) {
			if (__strcmp(iter->_name.c_str(), chain->c_str()) == 0) {
				//b_chain = true;
				tmp_sort.push_back(*iter);
				break;
			}
		}
	}
	
	//sorting 본격적으로 해야함...
	//std::sort(tmp_sort.begin(), tmp_sort.end());
	tmp_sort.sort();
	tmp_sort.reverse();
	chains.clear();
	for (std::list<D_adb_send_event_data>::iterator iter = tmp_sort.begin(); iter != tmp_sort.end(); iter++) {
		chains.push_back(iter->_name);
	}



	for (std::list<D_adb_send_event_data>::iterator iter = _data.begin(); iter != _data.end(); iter++) {
		//bool b_chain = false;
		for (std::list<std::string>::iterator chain = chains.begin(); chain != chains.end(); chain++) {
			if (__strcmp(iter->_name.c_str(), chain->c_str()) == 0) {
				//b_chain = true;
				iter->_chains = chains;
				break;
			}
		}
	}
}

int CD_adb_send_event::__touch_down(std::string name){
	for(std::list<D_adb_send_event_data>::iterator iter = _data.begin(); iter != _data.end(); iter++){
		if(__strcmp(iter->_name.c_str(), name.c_str()) != 0){
			continue;
		}
		if (iter->_slot != -1) {
			__touch_up(name);
		}

		iter->_slot = __get_empty_slot();
		OutputDebugString((std::string("touch_down__") + name + "\n").c_str());

		std::string str_write;
		str_write += std::string("/dev/input/event2 3 47 ") + __itos(iter->_slot);
		str_write += "\n";
		str_write += std::string("/dev/input/event2 3 57 ") + __itos(_tracking_id);
		str_write += "\n";
		str_write += std::string("/dev/input/event2 3 53 ") + __itos(iter->_x);
		str_write += "\n";
		str_write += std::string("/dev/input/event2 3 54 ") + __itos(iter->_y);
		str_write += "\n";
		str_write += "/dev/input/event2 3 48 1";
		str_write += "\n";
		str_write += "/dev/input/event2 3 49 1";
		str_write += "\n";
		str_write += std::string("/dev/input/event2 3 58 ") + __itos(iter->_pow);
		str_write += "\n";
		str_write += "/dev/input/event2 0 0 0";
		str_write += "\n";
		//send_processor.simple_write_input((char*)str_write.c_str());
		_str_send_event += str_write;
		
		_tracking_id++;
		if (_tracking_id > 65535) _tracking_id = 1;
		iter->_x_before = iter->_x;
		iter->_y_before = iter->_y;
		//iter->_pow++;
		iter->_state = eSTATE_RUN;
		break;
	}
	return 0;
}

int CD_adb_send_event::__touch_up(std::string name){
	for(std::list<D_adb_send_event_data>::iterator iter = _data.begin(); iter != _data.end(); iter++){
		if(__strcmp(iter->_name.c_str(), name.c_str()) != 0){
			continue;
		}
		if(iter->_slot == -1){
			return -1;
		}
		OutputDebugString((std::string("touch_up__") + name + "\n").c_str());
		std::string str_write;
		str_write += std::string("/dev/input/event2 3 47 ") + __itos(iter->_slot);
		str_write += "\n";
		str_write += "/dev/input/event2 3 57 -1";
		str_write += "\n";
		str_write += "/dev/input/event2 0 0 0";
		str_write += "\n";
		//send_processor.simple_write_input((char*)str_write.c_str());
		_str_send_event += str_write;

		iter->_slot = -1;
		iter->_pow = 1;
		iter->_state = eSTATE_NONE;
		break;
	}
	return 0;
}

int CD_adb_send_event::__touch_run(std::string name){
	for(std::list<D_adb_send_event_data>::iterator iter = _data.begin(); iter != _data.end(); iter++){
		if(__strcmp(iter->_name.c_str(), name.c_str()) != 0){
			continue;
		}
		if(iter->_slot == -1){
			return -1;
		}
		bool b_change_state = iter->_x != iter->_x_before || iter->_y != iter->_y_before;
		if (b_change_state) {
			OutputDebugString((std::string("touch_run__") + name + "\n").c_str());
			std::string str_write;
			str_write += std::string("/dev/input/event2 3 47 ") + __itos(iter->_slot);
			str_write += "\n";
			if (iter->_x != iter->_x_before) str_write += std::string("/dev/input/event2 3 53 ") + __itos(iter->_x);
			if (iter->_x != iter->_x_before) str_write += "\n";
			if (iter->_y != iter->_y_before) str_write += std::string("/dev/input/event2 3 54 ") + __itos(iter->_y);
			if (iter->_y != iter->_y_before) str_write += "\n";
			str_write += "/dev/input/event2 0 0 0";
			str_write += "\n";
			//send_processor.simple_write_input((char*)str_write.c_str());
			_str_send_event += str_write;
			iter->_x_before = iter->_x;
			iter->_y_before = iter->_y;
		}
		//iter->_pow++;
		break;
	}
	return 0;
}

int CD_adb_send_event::__touch_click(std::string name){
	if( !__touch_down(name) ){
		Sleep(1);
		if( !__touch_run(name) ){
			Sleep(1);
			return __touch_up(name);
		}
		OutputDebugString((std::string("touch_click__") + name + "\n").c_str());
	}
	return -1;
}

void CD_adb_send_event::__key_event_handling() {
	//키는 마지막에 눌려진 것을 가장 우선함
	//동일한 키를 사용하는 것들은 우선 순위에 따라 동작

	SHORT state = 0;
	for (std::list<D_adb_send_event_data>::iterator iter = _data.begin(); iter != _data.end(); iter++) {
		//chain 확인 후 다른 chain이 동작 중이라면 continue
		bool check_chain = false;
		for (std::list<std::string>::iterator chain = iter->_chains.begin(); chain != iter->_chains.end(); chain++) {
			if (get_state(*chain) != eSTATE_NONE && get_order(*chain) > iter->_order) {
				//OutputDebugString( ((iter->_name) + ":: " + *chain +  "|||FIRST||| \n").c_str() );
				check_chain = true;
				break;
			}
			//OutputDebugString( ((iter->_name) + ":: " + *chain + "\n").c_str() );
		}
		
		if (check_chain || iter->_keys.empty()) {
			//OutputDebugString("|||OUT|||\n");
			continue;
		}
		//모든 키가 0x8000 이거나 0x8001이라면 start
		bool key_on = false;
		for (std::list<int>::iterator iter_key = iter->_keys.begin(); iter_key != iter->_keys.end(); iter_key++){
			state = GetAsyncKeyState(*iter_key);
			if(state & 0x8000){
				//모든 키가 잘 동작 하는지 확인해야함
				key_on = true;
			}
			else if(state == 0x0001){
				key_on = false;
				if (iter->_state != eSTATE_END && iter->_state != eSTATE_NONE) {
					set_state(iter->_name, eSTATE_END);
				}
				break;
			}
			else if(state == 0x0000){
				key_on = false;
				if(iter->_state != eSTATE_END && iter->_state != eSTATE_NONE){
					set_state(iter->_name, eSTATE_END);
				}
				break;
			}
		}
		if (key_on) {
			set_state(iter->_name, eSTATE_START);
		}
	}
}

void CD_adb_send_event::__key_event_run() {
	for (std::list<D_adb_send_event_data>::iterator iter = _data.begin(); iter != _data.end(); iter++) {
		switch (iter->_state) {
		case eSTATE_NONE:
		{
			continue;
		}
		case eSTATE_CLICK:
		{
			__touch_click(iter->_name);
			continue;
		}
		case eSTATE_START:
		{
			if (!iter->_chains.empty()) {
				OutputDebugString((std::string("touch_chain__") + iter->_name + "\n").c_str());
				for (std::list<std::string>::iterator chain = iter->_chains.begin(); chain != iter->_chains.end(); chain++) {
					__touch_up(*chain);
					//if (__strcmp(iter->_name.c_str(), chain->c_str()) == 0) {
						//iter->_state = eSTATE_START;
					//}
				}
				Sleep(1);

			}
			__touch_down(iter->_name);
			continue;
		}
		case eSTATE_RUN:
		{
			__touch_run(iter->_name);
			continue;
		}
		case eSTATE_END:
		{
			__touch_up(iter->_name);
			continue;
		}
		}
	} //for(std::list<D_adb_send_event_data>::iterator iter = _data.begin(); iter != _data.end(); iter++){
}

void CD_adb_send_event::run(){
	static bool first_run = true;
	if (first_run) {
		_data.sort();
		first_run = false;
	}
	__key_event_handling();
	__key_event_run();
	if (!_str_send_event.empty()) {
		send_processor.simple_write_input((char*)_str_send_event.c_str());
	}
	_str_send_event.clear();
}


#endif /* _IN_CD_ADB_SEND_EVENT*/