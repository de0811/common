﻿/*++
    2019 / 1 / 15
--*/
#ifndef _IN_INC_UIAUTOMATOR
#define _IN_INC_UIAUTOMATOR


#undef UNICODE
#include "../common.h"
// #include <list>
// #include <string>
// #include "cd_log.h"
// #include "c_string.h"

#ifndef IN
#define IN  
#endif
#ifndef OUT
#define OUT  
#endif
#ifndef NULL
    #ifdef __cplusplus
        #define NULL 0
    #else
        #define NULL ((void *)0)
    #endif
#endif


class INC_uiautomator : protected CD_xml{
	std::string _device_id;
	std::pair<int, int> __get_box_point(std::pair<int, int> min, std::pair<int, int> max);
public:
	unsigned int parssing(std::string device_id);
	unsigned int button_click(std::list<std::pair<std::string, std::string>> find_list);
	unsigned int xiaomi_install_click();
};

unsigned int INC_uiautomator::xiaomi_install_click(){
	std::list<std::pair<std::string, std::string>> find_list;
	find_list.push_back( std::pair<std::string, std::string>( "resource-id", "com.android.packageinstaller:id/ok_button") );
	find_list.push_back( std::pair<std::string, std::string>( "clickable", "true") );
	return button_click(find_list);
}

std::pair<int, int> INC_uiautomator::__get_box_point(std::pair<int, int> min, std::pair<int, int> max){
	std::pair<int, int> result;
	result.first = ( (max.first - min.first) / 2 ) + min.first;
	result.second = ( (max.second - min.second) / 2 ) + min.second;
	return result;
}

unsigned int INC_uiautomator::button_click(std::list<std::pair<std::string, std::string>> find_list){
	std::list<CD_xml::D_element*> detect_list = CD_xml::find(find_list);
	if( detect_list.empty() ){
		return -1;
	}
	for(std::list<D_element*>::iterator iter = detect_list.begin(); iter != detect_list.end(); iter++){
		std::string bounds = (*iter)->find_attribute("bounds");
		if(bounds.empty()){
			continue;
		}
		//있다면
    	char* p_de_parenthesis = NULL;
    	unsigned int de_parenthesis_size = 0;
    	__str_replace((char*)bounds.c_str(), (char*)"][", (char*)" ", &p_de_parenthesis, &de_parenthesis_size);
		char* p_left_parenthesis = NULL;
		unsigned int left_parenthesis_size = 0;
		__str_replace(p_de_parenthesis, (char*)"[", (char*)"", &p_left_parenthesis, &left_parenthesis_size);
    	free(p_de_parenthesis);
		char* p_right_parenthesis = NULL;
		unsigned int right_parenthesis_size = 0;
		__str_replace(p_left_parenthesis, (char*)"]", (char*)"", &p_right_parenthesis, &right_parenthesis_size);
		free(p_left_parenthesis);

		char **p_space_str_split = NULL;
		unsigned int space_str_split_count = 0;
		__str_split(p_right_parenthesis, (char*)" ", &p_space_str_split, &space_str_split_count);	
		if(space_str_split_count != 2){
			__str_split_delete(&p_space_str_split, space_str_split_count);
			return -1;
		}
		char** p_min_coordinate = NULL;
		unsigned int min_coordinate_count = 0;
		__str_split(p_space_str_split[0], (char*)",", &p_min_coordinate, &min_coordinate_count);
		if(min_coordinate_count != 2){
			__str_split_delete(&p_space_str_split, space_str_split_count);
			__str_split_delete(&p_min_coordinate, min_coordinate_count);
			return -1;
		}
		
		char** p_max_coordinate = NULL;
		unsigned int max_coordinate_count = 0;
		__str_split(p_space_str_split[1], (char*)",", &p_max_coordinate, &max_coordinate_count);
		if(max_coordinate_count != 2){
			__str_split_delete(&p_space_str_split, space_str_split_count);
			__str_split_delete(&p_min_coordinate, min_coordinate_count);
			__str_split_delete(&p_max_coordinate, max_coordinate_count);
			return -1;
		}

		std::pair<int, int> min_point;
		if( !__isnumber(10, p_min_coordinate[0]) || !__isnumber(10, p_min_coordinate[1])){
			__str_split_delete(&p_max_coordinate, max_coordinate_count);
			__str_split_delete(&p_min_coordinate, min_coordinate_count);
			__str_split_delete(&p_space_str_split, space_str_split_count);
			return -1;
		}
		min_point.first = __atoi(p_min_coordinate[0]);
		min_point.second = __atoi(p_min_coordinate[1]);
		
		std::pair<int, int> max_point;
		if( !__isnumber(10, p_max_coordinate[0]) || !__isnumber(10, p_max_coordinate[1])){
			__str_split_delete(&p_max_coordinate, max_coordinate_count);
			__str_split_delete(&p_min_coordinate, min_coordinate_count);
			__str_split_delete(&p_space_str_split, space_str_split_count);
			return -1;
		}
		max_point.first = __atoi(p_max_coordinate[0]);
		max_point.second = __atoi(p_max_coordinate[1]);

		__str_split_delete(&p_max_coordinate, max_coordinate_count);
		__str_split_delete(&p_min_coordinate, min_coordinate_count);
		__str_split_delete(&p_space_str_split, space_str_split_count);

		std::pair<int, int> point = __get_box_point(min_point, max_point);

		char x[50] = {0,};
		char y[50] = {0,};
		_itoa(point.first, x, 10);
		_itoa(point.second, y, 10);

		std::string cmd = std::string("adb -s ") + _device_id + " shell input tap " + x + " " + y;
		CD_processor cd_processor;
		cd_processor.run(cmd.c_str());
		std::string full_str;
		std::string tmp_str;
		while( cd_processor.is_read() ){
			tmp_str = cd_processor.pop_data();
			if(tmp_str.empty()){
				continue;
			}
			full_str += tmp_str;
		}	
	}
	return 0;
}

unsigned int INC_uiautomator::parssing(std::string device_id){
	/*
	간혹 에러가 나오네
ERROR: null root node returned by UiTestAutomationBridge.
	성공 했을땐
	UI hierchary dumped to: /sdcard/vivi.xml
	*/
	_device_id = device_id;
	std::string cmd = std::string("adb -s ") + _device_id + " shell uiautomator dump /sdcard/vivi.xml";
	CD_processor cd_pro_uiauto;
	cd_pro_uiauto.run(cmd.c_str());
	std::string tmp_dump = "";
	std::string full_dump = "";
	while( cd_pro_uiauto.is_read() ){
		tmp_dump = cd_pro_uiauto.pop_data();
		if(tmp_dump.empty()) continue;
		full_dump += tmp_dump;
	}
	if(__strstr((char*)full_dump.c_str(), "UI hierchary dumped to: /sdcard/vivi.xml") == NULL){
		return -1;
	}

	//resource-id= com.android.packageinstaller:id/ok_button
	cmd = std::string("adb -s ") + _device_id + " shell cat /sdcard/vivi.xml";
	CD_processor cd_processor;
	cd_processor.run(cmd.c_str());
	std::string full_str;
	std::string tmp_str;
	while( cd_processor.is_read() ){
		tmp_str = cd_processor.pop_data();
		if(tmp_str.empty()){
			continue;
		}
		full_str += tmp_str;
	}
	if ( !full_str.empty() && __strstr((char*)full_str.c_str(), "No such file or directory") == NULL ) {
		CD_xml::parssing(full_str.c_str());
		return 0;
	}

	return -1;
}


#endif /* _IN_INC_UIAUTOMATOR*/