﻿

#ifndef _IN_CD_OPTION
#define _IN_CD_OPTION


#include <stdio.h>
#include <vector>
#include <map>
#include <algorithm>
#include "c_string.h"
#include "cd_log.h"

//args에서 command 별 인자들을 나누어줌
#include <list>
std::list< std::pair<const char*, std::list<const char*>> > __args_split(std::vector<const char*> args, std::list<const char*> options){
	// std::pair<const char*, int> point()
	std::list< std::pair<const char*, unsigned int> > points;
	for(unsigned int args_idx = 0; args_idx < args.size(); args_idx++){
		for(std::list<const char*>::iterator iter = options.begin(); iter != options.end(); iter++){
			if(__strstr((char*)args[args_idx], (char*)(*iter)) != NULL){
				points.push_back( std::make_pair(args[args_idx], args_idx) );
				break;
			} //if(__strstr((char*)args[args_idx], (char*)(*iter)) != NULL)
		} //for(std::list<const char*>::iterator iter = options.begin(); iter != options.end(); iter++)
	} //for(unsigned int args_idx = 0; args_idx < args.size(); args_idx++)

	std::list< std::pair<const char*, std::list<const char*>> > options_args;
	//어느 포인트까지 있는지는 나왔으니 이걸 기준으로 구간 별 나누기
	for(std::list< std::pair<const char*, unsigned int> >::iterator iter = points.begin(); iter != points.end(); iter++){
		std::list< std::pair<const char*, unsigned int> >::iterator n_iter = iter;
		n_iter++;
		std::list<const char*> option_args;
		if(n_iter == points.end()){
			//iter의 마지막 idx까지
			for(unsigned int forw = ( (*iter).second ); forw < args.size(); forw++ ){
				option_args.push_back( args[forw] );
			}
			options_args.push_back(std::pair<const char*, std::list<const char*>>((*iter).first, option_args));
			break;	//마지막이니까 탈출
		}	//if(n_iter == points.end())
		else{
			for(unsigned int forw = ( (*iter).second ); forw < (*n_iter).second; forw++){
				option_args.push_back( args[forw] );
			} //for(unsigned forw = (*iter).second; forw < (*n_iter).second; forw++)
			options_args.push_back(std::pair<const char*, std::list<const char*>>((*iter).first, option_args));
		}	//else
	}

	for(std::list< std::pair<const char*, std::list<const char*>> >::iterator iter = options_args.begin(); iter != options_args.end(); iter++){
		std::list< std::pair<const char*, std::list<const char*>> >::iterator n_iter = iter;
		n_iter++;
		for(; n_iter != options_args.end(); n_iter++){
			if(__strcmp((*iter).first, (*n_iter).first) == 0){
				(*n_iter).second.pop_front();
				(*iter).second.merge( (*n_iter).second );
				options_args.erase(n_iter);
				n_iter = iter;
				n_iter++;
			}
		}
	}


	for(std::list< std::pair<const char*, std::list<const char*>> >::iterator iter = options_args.begin(); iter != options_args.end(); iter++){
		CD_log::print(CD_log::LOG_DEBUG, 0, "%s\n", (*iter).first);
		for(std::list<const char*>::iterator args_iter = (*iter).second.begin(); args_iter != (*iter).second.end(); args_iter++){
			CD_log::print(CD_log::LOG_DEBUG, 1, "%s\n", (*args_iter));
		}
	}

    return options_args;
}

/**
 * C : Controller
 * D : Data
 * I : Interface
*/

/**
 * 규칙
 * OPT_TYPE::MAIN 의 인자 값은 arg_max_count 로 고정
 * OPT_TYPE::MAIN의 묵시적 사용은 가시적 사용이 없을 경우만 적용
 * OPT_TYPE::MAIN의 묵시적 사용은 인자의 가장 마지막을 취급
 * order는 값이 낮을 수록 먼저 실행
*/

class C_none_type{

};

class I_option_state{
public:
    enum{
        eSTATE_OK                     =0,
        eSTATE_ERROR                  =1,
        eSTATE_ERROR_ARGS_COUNT         ,
        eSTATE_ERROR_MAIN_OVERLAP       ,
        eSTATE_ERROR_NONE_MAIN          ,
        eSTATE_ERROR_OPTION_OVERLAP     ,
    };
    const char* get_error_log(unsigned int state);
};

const char* I_option_state::get_error_log(unsigned int state){
    switch(state){
        case eSTATE_OK                      :
        return "";
        case eSTATE_ERROR                   :
        return "eSTATE_ERROR";
        case eSTATE_ERROR_ARGS_COUNT        :
        return "eSTATE_ERROR_ARGS_COUNT";
        case eSTATE_ERROR_MAIN_OVERLAP      :
        return "eSTATE_ERROR_MAIN_OVERLAP";
        case eSTATE_ERROR_NONE_MAIN         :
        return "eSTATE_ERROR_NONE_MAIN";
        case eSTATE_ERROR_OPTION_OVERLAP    :
        return "eSTATE_ERROR_OPTION_OVERLAP";
        default:
        return "????";
    }
}

template <typename T>class CD_option : public I_option_state{
public:
enum OPT_TYPE{
    OPTION,      //HELP에서는 사용하지 않는 옵션
    MAIN,          //아무런 값 안넣어도 동작할 기본 옵션 (HELP가 끼어있지 않은 이상 무조건 실행)
};
protected:
    struct D_opt{
        const char*         str_opt;
        OPT_TYPE            etype;
        int        order;
        int        arg_min_count;
        int        arg_max_count;
        T*    run_class;
        unsigned int (T::*pfunc)(std::vector<const char*> args);
        unsigned int (*prun)(std::vector<const char*> args);

        bool operator <(const D_opt &prh){
            if(this->order < prh.order){
                return true;
            }
			if (this->order == prh.order) {
				if (this->etype == prh.etype) return false;
				return prh.etype == OPT_TYPE::MAIN ? true : false;
			}
            return false;
        }
    };

    std::vector<D_opt>                                  _opts;
    std::map<const char*, std::vector<const char*>>     _opt_args; //Parameter

public:


    unsigned int __option_overlap_check(std::vector<const char*> args);
    unsigned int add_opt(const char* str_opt, OPT_TYPE etype, int order, int arg_min_count, int arg_max_count, T* run_class, unsigned int (T::*pfunc)(std::vector<const char*> args), unsigned int (*prun)(std::vector<const char*> args));
    unsigned int parssing(std::vector<const char*> args);
    unsigned int parssing(int argc, char* argv[]);
    unsigned int run();
};



template<typename T> unsigned int CD_option<T>::__option_overlap_check(std::vector<const char*> args){
    /**
     * 옵션의 중복 사용 검사 && main이 되는 옵션이 2개 이상 사용 되었는지 검사
     * main이 아에 없다면 eSTATE_ERROR_NONE_MAIN 반환
     * 옵션의 중복이 있다면 eSTATE_ERROR_OPTION_OVERLAP 반환 -현재 잠궈둠
     * main의 옵션이 중복 사용 되었다면 eSTATE_ERROR_MAIN_OVERLAP 반환
     * 중복이 없다면 eSTATE_OK 반환
     * 
    */
    std::vector<D_opt> used_opts;

    used_opts.reserve(args.size());
    for(unsigned int i = 0; i < args.size(); i++){
        for(unsigned int k = 0; k < _opts.size(); k++){
            if( !__strcmp(args[i], _opts[k].str_opt) ){
                for(unsigned int o = 0; o < used_opts.size(); o++){
                    if( !__strcmp(used_opts[o].str_opt, _opts[k].str_opt) ){    //혹시 중복이 있다면 중지
                        return eSTATE_ERROR_OPTION_OVERLAP;
                    }
                }
                used_opts.push_back(_opts[k]);
            }
        }
    }

    bool bmain_overlap = false;
    for(unsigned int i = 0; i < used_opts.size(); i++){
        if( used_opts[i].etype == OPT_TYPE::MAIN ){
            if( bmain_overlap == true ){
                // return eSTATE_ERROR_MAIN_OVERLAP;    //MAIN OPTION 중복 안되도록 할 수 있음
            }
            bmain_overlap = true;
        }
    }

    if( !bmain_overlap ){
        for(unsigned int i = 0; i < _opts.size(); i++){
            if( _opts[i].etype == OPT_TYPE::MAIN && !__strcmp(_opts[i].str_opt, "") ){
                return eSTATE_OK;
            }
        }
        return eSTATE_ERROR_NONE_MAIN;
    }


    return eSTATE_OK;
}

/***
 * char*            str_opt     : 옵션으로 사용할 명령어
 * unsigned int     order       : 우선 순위(우선 순위 순서로 명령어 진행)
 * unsigned int     arg_count   : 최소 인자 갯수 또는 총 인자 갯수 (고정 값인지는 fixed_arg 값으로 판별)
 * bool             fixed_arg   : arg_count가 최소 인자 갯수인지 고정 값인지 나타냄
 * C_option_runner
*/
template<typename T> unsigned int CD_option<T>::add_opt(const char* str_opt, OPT_TYPE etype, int order, int arg_min_count, int arg_max_count, T* run_class, unsigned int (T::*pfunc)(std::vector<const char*> args), unsigned int (*prun)(std::vector<const char*> args)){
    D_opt data_opt;
    data_opt.str_opt = str_opt;
    data_opt.etype = etype;
    data_opt.order = order;
    if(arg_min_count > arg_max_count)   return eSTATE_ERROR_ARGS_COUNT;
    data_opt.arg_min_count = arg_min_count;
    data_opt.arg_max_count = arg_max_count;
    data_opt.run_class = run_class;
    data_opt.pfunc = pfunc;
    data_opt.prun = prun;
    if(data_opt.run_class != NULL){
        data_opt.prun = NULL;
    }

    _opts.reserve(_opts.size() + 1);
    _opts.push_back(data_opt);


    return eSTATE_OK;
}

/**
 * main으로 받아오는 인자를 바로 전달하면 됨
 * 
*/
template<typename T> unsigned int CD_option<T>::parssing(int argc, char* argv[]){
    std::vector<const char*> args;
    // CD_log::print(CD_log::LOG_ERROR, "argc :: %d\n", argc);
    if(argc != 0){
        args.reserve(argc - 1);
        for(int i = 1; i < argc; ++i){
            // CD_log::print(CD_log::LOG_ERROR, "arv %d :: %s\n", i - 1, argv[i]);
            args.push_back(argv[i]);
        }
    }
    parssing(args);
    return eSTATE_OK;
}

template<typename T> unsigned int CD_option<T>::parssing(std::vector<const char*> args){
    {
        unsigned int state = __option_overlap_check(args);
        if(state != eSTATE_OK){
            CD_log::print(CD_log::LOG_ERROR, "%s\n", get_error_log(state));
            return state;
        }
    }
 	//CD_log::print(CD_log::LOG_DEBUG, "args :: \n");
	for (unsigned int i = 0; i < args.size(); i++) {
		//CD_log::print(CD_log::LOG_DEBUG, "%s     ", args[i]);
	}
 	//CD_log::print(CD_log::LOG_DEBUG, "\n");
    std::sort(_opts.begin(), _opts.end());
    /*
    CD_log::print(CD_log::LOG_ERROR, "OPT_SORT\n");
    for(int i = 0; i < _opts.size(); i++){
        CD_log::print(CD_log::LOG_ERROR, "str_opt = %s  order[%d]\n", _opts[i].str_opt, _opts[i].order);
    }
    */

    std::vector<int> point;
    std::vector<D_opt> point_opts;
    point.reserve(_opts.size());
    point_opts.reserve(_opts.size());
    for(unsigned int k = 0; k < args.size(); k++){
        for(unsigned int i = 0; i < _opts.size(); i++){
            if( !__strcmp(_opts[i].str_opt, args[k]) ){
                point.push_back(k);
                point_opts.push_back(_opts[i]);
                // CD_log::print(CD_log::LOG_ERROR, "Add Point %d\n", k);
            }
        }
    }

    //MAIN을 찾자 (묵시적인지 가시적인지 확인)
    bool bexplicit = false;
    for(unsigned int i = 0; i < point_opts.size(); i++){
        if(point_opts[i].etype == OPT_TYPE::MAIN){
            bexplicit = true;
        }
    }
    //묵시적 사용일 경우 맨 뒤에서 잘라야함
    if(bexplicit == false){
        for(unsigned int i = 0; i < _opts.size(); i++){
            if( !__strcmp(_opts[i].str_opt, "") && _opts[i].etype == OPT_TYPE::MAIN ){
                int default_idx = args.size() - _opts[i].arg_max_count;
                // CD_log::print(CD_log::LOG_ERROR, "default add point %d\n", default_idx);
                // CD_log::print(CD_log::LOG_ERROR, "default ?? args.size(%d) arg_max_count[%d]\n", args.size(), _opts[i].arg_max_count);
                // CD_log::print(CD_log::LOG_ERROR, " point empty %d  point last index [%d]\n", point.empty(), point.empty()? 0 : point[point.size()-1]);
                if( (point.empty() ? 0 : point[ point.size()-1 ] >= default_idx) || default_idx < 0 ){ //다른 옵션이 있는데 묵시적 옵션이 그 뒤로 포인트가 잡히는 경우(다른 포인터를 포함할 경우도 에러)
                    if(point.empty() ? 0 : point[ point.size()-1 ] >= default_idx)
                        CD_log::print(CD_log::LOG_ERROR, "%s __OPTION[default] option collision - lack %d\n", get_error_log(eSTATE_ERROR_ARGS_COUNT), default_idx - point[ point.size()-1 ] - _opts[i].arg_max_count );
                    else if(default_idx > 0)
                        CD_log::print(CD_log::LOG_ERROR, "%s __OPTION[default] over %d\n", get_error_log(eSTATE_ERROR_ARGS_COUNT), default_idx);
                    else if(default_idx < 0)
                        CD_log::print(CD_log::LOG_ERROR, "%s __OPTION[default] lack %d\n", get_error_log(eSTATE_ERROR_ARGS_COUNT), default_idx);
                    return eSTATE_ERROR_ARGS_COUNT;
                }
                point.push_back(default_idx);
                point_opts.push_back(_opts[i]);
            }
        }
    }
    //가시적(명시적) 이라면 잘라놓은 포인트 대로 잘라버리면 됨 묵시적의 경우도 묵시적 상태의 것만 따로 작업 후 이후는 동일하게 처리하도록 진행
    {
        int start_idx = 0;
        int end_idx = 0;   //end_idx는 다음 idx를 가르킨다 있든 없든

        for(unsigned int i = 0; i < point.size(); i++){  //찾은 옵션 횟수만큼
            //-----------start_idx 와 end_idx 값 찾기------------------
            D_opt &opt_info = point_opts[i];
            start_idx = point[i];
            if(point.size() <= i + (unsigned int)1){
                end_idx = args.size();
            }else{
                end_idx = point[i + 1];
            }
            // CD_log::print(CD_log::LOG_ERROR, "ARG PARSSING opt[%s] start_idx[%d] end_idx[%d]\n", opt_info.str_opt, start_idx, end_idx);
            //--------------------------------------------------------

            //------------묵시적 처리의 경우 인자 갯수가 제대로 되었는지 확인---------------
            if( opt_info.etype == OPT_TYPE::MAIN && !__strcmp(opt_info.str_opt, "")){ //묵시적 main 옵션이라면
                if( opt_info.arg_max_count != end_idx - start_idx){
                    // CD_log::print(CD_log::LOG_ERROR, "arg_max_count %d   start_idx %d  end_idx %d\n", opt_info.arg_max_count, start_idx, end_idx);
                    if(opt_info.arg_max_count - (end_idx + start_idx) > 0)
                        CD_log::print(CD_log::LOG_ERROR, "%s OPTION[default] lack %d \n", get_error_log(eSTATE_ERROR_ARGS_COUNT), opt_info.arg_max_count - (end_idx + start_idx));
                    if(opt_info.arg_max_count - (end_idx + start_idx) < 0)
                        CD_log::print(CD_log::LOG_ERROR, "%s OPTION[default] over %d \n", get_error_log(eSTATE_ERROR_ARGS_COUNT), opt_info.arg_max_count - (end_idx + start_idx));
                    return eSTATE_ERROR_ARGS_COUNT;
                }
            }
            //-------------------------------------------------------------------------
            else{   //그 이외의 모든 옵션
                int arg_count = end_idx - start_idx - 1; //자기 자신 갯수는 빼야지
                //------------------가시적 MAIN의 경우 인자 갯수가 제대로 되었는지 확인--------------
                if(opt_info.etype == OPT_TYPE::MAIN){
                    if(arg_count != opt_info.arg_max_count){
                        if(opt_info.arg_max_count - arg_count > 0)
                            CD_log::print(CD_log::LOG_ERROR, "%s MAIN_OPTION[%s] lack %d \n", get_error_log(eSTATE_ERROR_ARGS_COUNT), opt_info.str_opt, arg_count - opt_info.arg_max_count);
                        if(opt_info.arg_max_count - arg_count < 0)
                            CD_log::print(CD_log::LOG_ERROR, "%s MAIN_OPTION[%s] over %d \n", get_error_log(eSTATE_ERROR_ARGS_COUNT), opt_info.str_opt, arg_count - opt_info.arg_max_count);
                        return eSTATE_ERROR_ARGS_COUNT;
                    }
                }
                //-------------------------------------------------------------------------------
                //------------------------일반 옵션의 경우 인자 갯수가 제대로 되었는지 확인----------
                else if(arg_count < opt_info.arg_min_count){
                    CD_log::print(CD_log::LOG_ERROR, "%s OPTION[%s] lack %d \n", get_error_log(eSTATE_ERROR_ARGS_COUNT), opt_info.str_opt, arg_count - opt_info.arg_min_count);
                    return eSTATE_ERROR_ARGS_COUNT;
                }
                else if(arg_count > opt_info.arg_max_count){
                    CD_log::print(CD_log::LOG_ERROR, "%s OPTION[%s] over %d \n", get_error_log(eSTATE_ERROR_ARGS_COUNT), opt_info.str_opt, arg_count - opt_info.arg_max_count);
                    return eSTATE_ERROR_ARGS_COUNT;
                }
                //-------------------------------------------------------------------------------
            }

            if( start_idx > end_idx ){
                CD_log::print(CD_log::LOG_ERROR, "%s\n", get_error_log(eSTATE_ERROR_ARGS_COUNT));
                return eSTATE_ERROR_ARGS_COUNT;
            }

            // CD_log::print(CD_log::LOG_ERROR, "point : %d  start_idx[%d], end_idx[%d]\n", point[i], start_idx, end_idx);
            // _opt_args.insert()
            std::vector<const char*> temp_vec;
            if( opt_info.etype == OPT_TYPE::MAIN && !__strcmp(opt_info.str_opt, "")){ //묵시적 main 옵션이라면
                temp_vec.reserve(end_idx - start_idx);
                for(int k = start_idx; k < end_idx; k++){
                    temp_vec.push_back(args[k]);
                }
            }
            else{
                temp_vec.reserve(end_idx - start_idx - 1);
                for(int k = start_idx + 1; k < end_idx; k++){
                    temp_vec.push_back(args[k]);
                }
            }
            _opt_args.insert(std::make_pair(opt_info.str_opt, temp_vec));
        }

        //혹시 남는 args가 있다면 error 반환
    }

   
    //일단 옵션인 것만 다 나눈다
    //map key list으로 옵션인 부분만 리스트 따로 모으기
    //옵션이 몇번째에 있는지들 다 확인

    //일단 옵션에 따라 옵션들 다 나누기



    return eSTATE_OK;
}

template <typename T> unsigned int CD_option<T>::run(){
    unsigned int result = 0;
    for(unsigned int i = 0; i < _opts.size(); i++){
        std::map<const char*, std::vector<const char*>>::iterator iter = _opt_args.find(_opts[i].str_opt);
        if(iter != _opt_args.end()){
            if( _opts[i].run_class != NULL && _opts[i].pfunc != NULL ){
                ((_opts[i].run_class)->*(_opts[i].pfunc))(iter->second);
            }
            else if( _opts[i].prun != NULL ){
                result = (*_opts[i].prun)(iter->second);
            }
            if( result != eSTATE_OK ){
                //----------- Error나 중지 신청일 때 뭘 날릴텐데....... 어쩌지
                CD_log::print(CD_log::LOG_ERROR, "%s run()\n", get_error_log(result));
                return result;
            }
        }
    }
    return eSTATE_OK;
}





#endif /*  _IN_CD_OPTION */