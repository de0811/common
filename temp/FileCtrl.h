//
// Created by dmseo on 2017-07-18.
//
#define WIN32

#ifdef WIN32
#define _CRT_SECURE_NO_WARNINGS
#endif

#ifdef WIN32
#else
#include <android/log.h> //log
#endif //WIN32




#include <string>

#ifdef WIN32
#include <Windows.h>
#else
#include <dirent.h>
#endif //WIN32

#include <vector>
#include <list>

#include  <sys/stat.h>

#ifdef WIN32
#else
#include  <unistd.h>
#endif //WIN32


#include <fcntl.h>
#ifdef WIN32
#include <io.h>
#else
#endif //WIN32


#ifndef MAKE_UTILL_FILECTRL_H
#define MAKE_UTILL_FILECTRL_H

#endif //MAKE_UTILL_FILECTRL_H

#ifdef WIN32
const std::string SEP = "\\";
#else
const std::string SEP = "/";
#endif //WIN32

#ifdef WIN32
#define open _open
#define lseek _lseek
#define read _read
#define close _close
#else
#endif //WIN32






//파일/폴더 목록 확인
std::vector<std::string> getFiles(std::string);

//파일 정보 확인
unsigned int getFileType(std::string strFile);

//파일 크기
size_t getFileSize(int hFile);
size_t getFileSize(std::string strFile);




void readFile(std::string strFile, char* &pBuf, size_t &size) {
	size = getFileSize(strFile);
	int hFile = open(strFile.c_str(), O_RDONLY);
	pBuf = new char[size];
	read(hFile, pBuf, size);
}

size_t getFileSize(int hFile) {
	if (hFile == -1 || hFile == 0) return 0;
	size_t size = (size_t)lseek(hFile, 0, SEEK_END);
	lseek(hFile, 0, SEEK_SET);
	return size;
}

size_t getFileSize(std::string strFile) {
#ifdef WIN32
	if (strFile.empty()) return 0;

	int hFile = open(strFile.c_str(), O_RDONLY);

	if (hFile == -1)
		close(hFile);

	return getFileSize(hFile);
#else
	struct stat fstat;
	int ic;

	ic = lstat(strFile, &fstat);
	if (ic < 0)
	{
		return (-1);
	}

	return (int)fstat.st_size;
#endif //WIN32
}

/*
struct dirent {
long              d_ino;        // inode 번호
__kernel_off_t    d_off;         // 디렉토리 내에서의 오프셋
unsigned short  d_reclen;     // 엔트리의 길이
unsigned char    d_type;         //데이터 타입 ( DT_FIFO )
char              d_name[256];  // 파일이나 디렉토리 이름
};

enum
{
DT_UNKNOWN = 0,
# define DT_UNKNOWN DT_UNKNOWN
DT_FIFO = 1,
# define DT_FIFO    DT_FIFO
DT_CHR = 2,
# define DT_CHR     DT_CHR
DT_DIR = 4,
# define DT_DIR     DT_DIR
DT_BLK = 6,
# define DT_BLK     DT_BLK
DT_REG = 8,
# define DT_REG     DT_REG
DT_LNK = 10,
# define DT_LNK     DT_LNK
DT_SOCK = 12,
# define DT_SOCK    DT_SOCK
DT_WHT = 14
# define DT_WHT     DT_WHT
};
*/
//폴더 목록 확인

inline bool ends_with(std::string const & value, std::string const & ending)
{
	if (ending.size() > value.size()) return false;
	return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

inline bool starts_with(const std::string& value, const std::string& ending) {
	return ending.length() <= value.length()
		&& equal(ending.begin(), ending.end(), value.begin());
}

std::vector<std::string> getFiles(std::string strPath) {
	std::vector<std::string> files;
	std::list<std::string> folders;
	if (strPath.empty()) return files;
	if (getFileType(strPath) != S_IFDIR) return files;

	folders.push_back(strPath);

#ifdef WIN32
	_finddata_t fd;
	long handle;
	int result = 1;
	std::string join = "";
	while (!folders.empty()) {
		std::string folder = folders.front();
		folders.pop_front();
		
		std::string find = "";
		if( ends_with(folder, SEP) ) find = folder + "*.*";
		else find = folder + SEP + "*.*";
		
		handle = _findfirst(find.c_str(), &fd);
		printf("find : %s \n", find.c_str());
		//현재 폴더 내 모든 파일을 찾는다.

		result = 1;

		while (result != -1)
		{
			printf("File: %s : %d \n", fd.name, getFileType(fd.name) == S_IFDIR);
			if (ends_with(folder, SEP)) join = folder;
			else join = folder + SEP;
			if (getFileType(join + fd.name) == S_IFDIR) {
				if (!strcmp(fd.name, ".") || !strcmp(fd.name, "..")) {
					result = _findnext(handle, &fd);
					continue;
				}
				if (ends_with(folder, SEP)) folders.push_back(folder + fd.name);
				else  folders.push_back(folder + SEP + fd.name);
				printf("keep folder : %s\n", std::string(folder + SEP + fd.name).c_str());
			}
			else {
				if (ends_with(folder, SEP)) files.push_back(folder + fd.name);
				else  files.push_back(folder + SEP + fd.name);
			}
			result = _findnext(handle, &fd);
		}

		_findclose(handle);
	}

#else
	DIR *dir_info;
	struct dirent *dir_entry;

	while (!folders.empty()) {
		std::string folder = folders.front();
		folders.pop_front();
		dir_info = opendir(folder.c_str());              // 현재 디렉토리를 열기
		if (NULL != dir_info)
		{
			while ((dir_entry = readdir(dir_info))) { // 디렉토리 안에 있는 모든 파일과 디렉토리 출력
				if (!strcmp(dir_entry->d_name, ".") || !strcmp(dir_entry->d_name, "..")) continue;
				if (dir_entry->d_type == DT_DIR) {
					folders.push_back(folder + SEP + dir_entry->d_name);
				}
				else {
					files.push_back(folder + SEP + dir_entry->d_name);
					__android_log_print(3, "SDM", "File : %s", std::string(folder + SEP + dir_entry->d_name).c_str());
				}
			}
			closedir(dir_info);
		}
	}
#endif //WIN32


	return files;
}

unsigned int getFileType(std::string strFile) {

	//#define S_ISLNK(m) (((m) & S_IFMT) == S_IFLNK)
	//#define S_ISREG(m) (((m) & S_IFMT) == S_IFREG)
	//#define S_ISDIR(m) (((m) & S_IFMT) == S_IFDIR)
	//#define S_ISCHR(m) (((m) & S_IFMT) == S_IFCHR)
	//#define S_ISBLK(m) (((m) & S_IFMT) == S_IFBLK)
	//#define S_ISFIFO(m) (((m) & S_IFMT) == S_IFIFO)
	//#define S_ISSOCK(m) (((m) & S_IFMT) == S_IFSOCK)
	/*
	struct stat buf = getFileInfo(strFile);

	switch ( buf.st_mode & S_IFMT ){
	case S_IFLNK :  //링크 파일
	break;
	case S_IFREG :  //일반 파일
	break;
	case S_IFDIR :  //디렉토리 파일
	break;
	case S_IFCHR :  //문자 파일
	break;
	case S_IFBLK :  //블록 파일
	break;
	case S_IFIFO :  //파이프 파일
	break;
	case S_IFSOCK : //소켓 파일
	break;

	}
	*/
#ifdef ________________
	DWORD attr = ::GetFileAttributes(strFile.c_str());
	int result = 0;
	if (attr != 0xFFFFFFFFL) {
		if (attr == FILE_ATTRIBUTE_DIRECTORY) result = S_IFDIR;
		else result = S_IFREG;

	}
	else {
		//return (false);
	}
	return result;
#else
	struct stat buf;

	stat(strFile.c_str(), &buf);

	return buf.st_mode & S_IFMT;
#endif // WIN32

	
}