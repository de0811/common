#include <iostream>
#include <windows.h>
#include <new>
/***
 * 2011년 4월 어느 날
 * 
 * 
*/

template <typename T> class Duble_List{
private :
	struct NODE {
		T data;
		NODE *NextPoint;
		NODE *BackPoint;	};
	NODE *SEED;
	NODE *END;

public :
	class interater{
	private :
		friend class Duble_List<T>;
		struct Secret{T *DATA;/* void Set(T &data){DATA = &data;}*/ };
		Secret Scr;
		T Null;
		///////////////////////////////////////////////////////
		NODE *CurrentNODE;
	public :
		interater() { CurrentNODE = NULL; memset(&Null, 0, sizeof(T)); }
		interater(NODE *Node) { CurrentNODE = Node; }
		T GetData() { 
			if(CurrentNODE)
				return CurrentNODE->data;
			else
				return NULL;
		}
		//operater ++ () //다음 노드로 가기
		interater operator ++(int) {
			interater tmpitr;
			if(CurrentNODE)
			{
				tmpitr.CurrentNODE = CurrentNODE;
				CurrentNODE = CurrentNODE->NextPoint;
			}
			return tmpitr;
		}

		interater& operator ++(){
			if(CurrentNODE)
				CurrentNODE = CurrentNODE->NextPoint;
			return *this;
		}
		//operater -- () //이전 노드 가기
		interater operator --(int) {
			interater tmpitr;
			if(CurrentNODE)
			{
				tmpitr.CurrentNODE = CurrentNODE;
				CurrentNODE = CurrentNODE->BackPoint;
			}
			return tmpitr;
		}

		interater& operator --(){
			if(CurrentNODE)
				CurrentNODE = CurrentNODE->BackPoint;
			return *this;
		}
		//operater -> DATA만 나오게, 다른 포인터는 보이지 않게 하기
		Secret* operator ->()
		{ 
			if(CurrentNODE)
				Scr.DATA = &CurrentNODE->data;
			else
				Scr.DATA = &Null;
			return &Scr;
		}
	};
	Duble_List() { SEED = NULL, END = NULL;}
	~Duble_List() { Clear(); }
	void GetSeed(interater &itr) { itr.CurrentNODE = SEED; printf("SEED = %d\n", SEED);}	//처음 노드를 반환
	void Insert(T data);	//SEED 앞에 붙이기
	void Next_Add(T data, interater itr);
	void Back_Add(T data, interater itr);
	int Size();	//노드 갯수
	void Delete(interater itr);	//해당 노드 삭제
	void Clear();	//모두 지우기
};


template <typename T>
void Duble_List<T>::Insert(T data)
{
	NODE *NewNode;
	NewNode = new NODE;
	NewNode->NextPoint = NULL;
	NewNode->BackPoint = NULL;
	memcpy(&NewNode->data, &data, sizeof(T));

	printf("=====Insert=====\n");
	if(SEED)	//노드가 하나라도 있다면
	{
		NewNode->NextPoint = SEED;
		SEED->BackPoint = NewNode;
		SEED = NewNode;
		printf("Midle\n");
	}
	else	//처음 이라면
	{
		SEED = NewNode;
		END = NewNode;
		printf("first \n");
	}
	printf("Addreass : %d \n", NewNode);
	printf("Next Addreass : %d \n", NewNode->NextPoint);
	printf("Back Addreass : %d \n", NewNode->BackPoint);
	printf("================\n");
}

template <typename T>
void Duble_List<T>::Next_Add(T data, interater itr)
{
	if(!itr.CurrentNODE)
	{
		Insert(data);
		return;
	}

	printf("Next_Add\n");
	NODE *NewNode;
	NewNode = new NODE;
	NewNode->NextPoint = NULL;
	NewNode->BackPoint = NULL;
	memcpy(&NewNode->data, &data, sizeof(T));
	printf("Addreass : %d \n", NewNode);

	if(itr.CurrentNODE)
	{
		NewNode->BackPoint = itr.CurrentNODE;
		NewNode->NextPoint = itr.CurrentNODE->NextPoint;
		itr.CurrentNODE->NextPoint = NewNode;
		if(!(NewNode->NextPoint)) END = NewNode;
	}
}

template <typename T>
void Duble_List<T>::Back_Add(T data, interater itr)
{
	if(!itr.CurrentNODE)
	{
		Insert(data);
		return;
	}

	printf("Next_Add\n");
	NODE *NewNode;
	NewNode = new NODE;
	NewNode->NextPoint = NULL;
	NewNode->BackPoint = NULL;
	memcpy(&NewNode->data, &data, sizeof(T));
	printf("Addreass : %d \n", NewNode);

	if(itr.CurrentNODE)
	{
		NewNode->NextPoint = itr.CurrentNODE;
		NewNode->BackPoint = itr.CurrentNODE->BackPoint;
		itr.CurrentNODE->BackPoint = NewNode;
		if(!(NewNode->BackPoint)) SEED = NewNode;
	}
}

template <typename T>
int Duble_List<T>::Size()
{
	NODE *List = SEED;
	int rtn = 0;
	while(List)
	{
		rtn++;
		List = List->NextPoint;
	}
	return rtn;
}

template <typename T>
void Duble_List<T>::Delete(interater itr)
{
	printf("=====Delete=====\n");
	printf("Addreass : %d \n", itr.CurrentNODE);
	if(itr.CurrentNODE)	//없는데 지우라 하면
	{
		if((itr.CurrentNODE->NextPoint) && (itr.CurrentNODE->BackPoint))		//양쪽으로 있는가
		{
			printf("Next Back OK\n");
			itr.CurrentNODE->NextPoint->BackPoint = itr.CurrentNODE->BackPoint;
			itr.CurrentNODE->BackPoint->NextPoint = itr.CurrentNODE->NextPoint;
		}
		else if((itr.CurrentNODE->NextPoint) && (!itr.CurrentNODE->BackPoint)) //내가 SEED다
		{
			printf("Back SEED\n");
			SEED = itr.CurrentNODE->NextPoint;
			itr.CurrentNODE->NextPoint->BackPoint = NULL;
		}
		else if((!itr.CurrentNODE->NextPoint) && (itr.CurrentNODE->BackPoint)) //내가 END다
		{
			printf("Next END\n");
			END = itr.CurrentNODE->BackPoint;
			itr.CurrentNODE->BackPoint->NextPoint = NULL;
		}
		else if((!itr.CurrentNODE->NextPoint) && (!itr.CurrentNODE->BackPoint)) //노드가 current밖에 없음
		{
			printf("Current Only\n");
			SEED = NULL;
			END = NULL;
		}
		delete itr.CurrentNODE;
	}
	printf("================\n");
}

template <typename T>
void Duble_List<T>::Clear()
{
	printf("=====Clear=====\n");
	while(SEED)
	{
		NODE *Delete_Node;
		Delete_Node = SEED;
		SEED = Delete_Node->NextPoint;
		printf("Addreass : %d \n", Delete_Node);
		printf("Next Addreass : %d \n", Delete_Node->NextPoint);
		printf("Back Addreass : %d \n", Delete_Node->BackPoint);
		delete Delete_Node;
	}
	printf("================\n");
}
