
// #include <stdio.h>


#ifndef _IN_FILE
#define _IN_FILE

#ifdef  __cplusplus
extern "C" {
#endif


#ifndef IN
#define IN  
#endif
#ifndef OUT
#define OUT  
#endif


//----------------------------------------------------------------------------------------------------------------------
// get_file_size
//file size 반환
//
//----------------------------------------------------------------------------------------------------------------------
/**
 * __get_file_size
 * 문제 사항
 * /proc/ 하위의 파일들처럼 사이즈가 0으로 표시되는 파일의 경우 직접 내용을 읽어서 크기를 알아내는 방법 밖에 없음
 * 어떤 파일인지 확인 후 사이즈가 0이 나오는 파일의 경우 고정 변수로 데이터를 받아와야함
 * 
 */
static
unsigned int __get_file_size(const char *_file_path)
{
	FILE* pf = fopen(_file_path, "rb");
	if (!pf) return 0;

	fseek(pf, 0, SEEK_END);
	unsigned int size = ftell(pf);

	fclose(pf);

	return size;
}

#ifdef  __cplusplus
}
#endif

#endif  /* _IN_FILE */