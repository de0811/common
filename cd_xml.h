﻿/*++
    2019 / 1 / 14
--*/
#ifndef _IN_CD_XML
#define _IN_CD_XML


#undef UNICODE
#include "common.h"
#include <list>
#include <string>
#include "cd_log.h"
#include "c_string.h"

#ifndef IN
#define IN  
#endif
#ifndef OUT
#define OUT  
#endif
#ifndef NULL
    #ifdef __cplusplus
        #define NULL 0
    #else
        #define NULL ((void *)0)
    #endif
#endif



/*
int main(int argc, char* argv[]){
	CD_xml xml;
	std::string cmd = std::string("adb shell cat /sdcard/vivi.xml");
	CD_log::print(CD_log::LOG_DEBUG, "cmd :: %s\n", cmd.c_str());

	CD_processor cd_processor;
	cd_processor.run(cmd.c_str());
	std::string full_str;
	std::string tmp_str;
	while( cd_processor.is_read() ){
		tmp_str = cd_processor.pop_data();
		if(tmp_str.empty()){
			continue;
		}
		full_str += tmp_str;
	}
	if ( !full_str.empty() && __strstr((char*)full_str.c_str(), "No such file or directory") == NULL ) {
		xml.parssing(full_str.c_str());
	}
	system("PAUSE");
	return 0;
}
*/



class CD_xml{
public:
	class D_element{
	public:
		enum TAG_TYPE{
			DECLARATION, //선언
			DEFINITION_NO_GROUP_TAG, //
			DEFINITION_GROUP_TAG,
			COMMENT, //주석
		};
		TAG_TYPE e_tag_type;
		std::string name;
		std::list<std::pair<std::string, std::string>> attribute;
		std::string data;
		D_element* p_parent;
		std::list<D_element*> childs;
		void print(int deep){
			CD_log::print(CD_log::LOG_DEBUG, deep, "%d  NAME[%s]\n", deep, name.c_str());
			CD_log::print(CD_log::LOG_DEBUG, deep, "%d  Attribute\n", deep);
			for(std::list<std::pair<std::string, std::string>>::iterator iter = attribute.begin(); iter != attribute.end(); iter++){
				CD_log::print(CD_log::LOG_DEBUG, deep, "%d      ATTR[%s] :: VAL[%s]\n", deep, iter->first.c_str(), iter->second.c_str());
			}
			for(std::list<D_element*>::iterator iter = childs.begin(); iter != childs.end(); iter++){
				(*iter)->print(deep + 1);
			}
		}

        std::string find_attribute(std::string key){
            for(std::list<std::pair<std::string, std::string>>::iterator iter = attribute.begin(); iter != attribute.end(); iter++){
                if(__strcmp(iter->first.c_str(), key.c_str()) == 0){
                    return iter->second;
                }
            }
            return "";
        }

		//child를 하기에는 모든 element를 비교할 예정이기에 구조가 달라지는 경우에는 별 의미가 없어보임
		bool equal(D_element* p_element){
			if( __strcmp(this->name.c_str(), p_element->name.c_str()) != 0
			|| __strcmp(this->data.c_str(), p_element->data.c_str()) != 0 
			|| this->attribute.size() != p_element->attribute.size() ){
			// || this->childs.size() != p_element->childs.size() ){
				return false;
			}
			std::list<std::pair<std::string, std::string>>::iterator this_iter = this->attribute.begin();
			std::list<std::pair<std::string, std::string>>::iterator el_iter = p_element->attribute.begin();
			while(this_iter != this->attribute.end() && el_iter != p_element->attribute.end()){
				if( __strcmp( this_iter->first.c_str(), el_iter->first.c_str() ) != 0 || __strcmp(this_iter->second.c_str(), el_iter->second.c_str()) != 0 ){
					return false;
				}
				this_iter++;
				el_iter++;
			}

			return true;
		}
	};

private:
    //tree 형태로 element를 정리 함
	std::list<D_element*> _elements;
    //순서에 상관 없이 1차원으로 element들을 모음
    std::list<D_element*> _element_list;

	//데이터를 한줄로 다 나누어서 읽을 수 있도록 해줌
	unsigned int __parssing_init(OUT std::list<std::string> &data_list, IN const char* data);
	//<, <?, <!--, >, />, ?>, --> 들을 제외한 나머지 순수 데이터 부분을 넣어주는 부분
	unsigned int __parssing_one_line(char* one_line, D_element *p_element);
	//<인지 <?인지 등등 인지 확인 후 처리
	D_element* __parssing(std::list<std::string> &data_list, std::list<std::string>::iterator &current_iter, D_element *p_parent);

public:
    std::list<CD_xml::D_element*> find(std::list<std::pair<std::string, std::string>> &find_list);
	unsigned int parssing(const char* data);
	void print();
	bool operator==(const CD_xml &xml) const;
    ~CD_xml();
};

bool CD_xml::operator==(const CD_xml &xml) const{
	return false;
}

CD_xml::~CD_xml(){
    for(std::list<D_element*>::iterator iter = _element_list.begin(); iter != _element_list.end(); iter++){
        delete *iter;
    }
}

void CD_xml::print() {
	for(std::list<D_element*>::iterator iter = _elements.begin(); iter != _elements.end(); iter++){
		(*iter)->print(0);
	}
}

std::list<CD_xml::D_element*> CD_xml::find(std::list<std::pair<std::string, std::string>> &find_list){
    std::list<CD_xml::D_element*> result;
    bool check = false;
	bool next_element = false;
    for(std::list<D_element*>::iterator el_iter = _element_list.begin(); el_iter != _element_list.end(); el_iter++){
//NEXT_ELEMENT:
        for(std::list<std::pair<std::string, std::string>>::iterator find_iter = find_list.begin(); find_iter != find_list.end(); find_iter++){
            for(std::list<std::pair<std::string, std::string>>::iterator attr_iter = (*el_iter)->attribute.begin(); attr_iter != (*el_iter)->attribute.end(); attr_iter++){
                if(__strcmp(find_iter->first.c_str(), attr_iter->first.c_str()) == 0 && __strcmp(find_iter->second.c_str(), attr_iter->second.c_str()) == 0){
                    check = true;
                }
            } //for(std::list<std::pair<std::string, std::string>>::iterator attr_iter = (*el_iter)->attribute.begin(); attr_iter != (*el_iter)->attribute.end(); attr_iter++){
            //attribute하나 확인 결과 1개는 없다? 있다?
            if(check == false){
				next_element = true;
				break;
                //goto NEXT_ELEMENT;
            }
            check = false;
        } //for(std::list<std::pair<std::string, std::string>>::iterator find_iter = find_list.begin(); find_iter != find_list.end(); find_iter++){
		if (next_element) {
			next_element = false;
			continue;
		}
        result.push_back(*el_iter);
    } //for(std::list<D_element*>::iterator el_iter = _element_list.begin(); el_iter != _element_list.end(); el_iter++){
    return result;
}

unsigned int CD_xml::__parssing_init(OUT std::list<std::string> &data_list, IN const char* data){
	char **pfull_str_split = NULL;
	unsigned int full_str_split_count = 0;

	__str_split_right((char*)data, (char*)">", &pfull_str_split, &full_str_split_count);

	for(unsigned int i = 0; i < full_str_split_count; i++){
		// printf("LINE::%s\n", pfull_str_split[i]);
		char **p_left_split = NULL;
		unsigned int left_split_count = 0;

		__str_split_left(pfull_str_split[i], (char*)"<", &p_left_split, &left_split_count);
		for(unsigned int k = 0; k < left_split_count; k++){
			data_list.push_back(std::string(p_left_split[k]));
		}

		__str_split_delete(&p_left_split, left_split_count);
	}

	__str_split_delete(&pfull_str_split, full_str_split_count);
	return 0;
}

unsigned int CD_xml::parssing(const char* data){
	// printf("%s\n", data);
	if ( data == NULL && __strstr((char*)data, "No such file or directory") != NULL ) {
		return 0;
	}
	std::list<std::string> data_list;

	__parssing_init(data_list, data);

	for(std::list<std::string>::iterator iter = data_list.begin(); iter != data_list.end(); iter++){
		// D_element *element = new D_element();
		//여기서 짤라 갔어염
		//__parssing(data_list, iter, NULL);
		D_element* p_element = __parssing(data_list, iter, NULL);
		if (p_element != NULL) {
			_elements.push_back( p_element );
			//p_element->print(0);
		}
		// _elements.push_back(element);
	}


/*
	print();
*/
	

	return 0;
}

//p_parent가 필요한 이유는 순전히 data를 넣어주기 위해서 필요함...
CD_xml::D_element* CD_xml::__parssing(std::list<std::string> &data_list, std::list<std::string>::iterator &current_iter, D_element *p_parent){
	/*
	CD_log::print(CD_log::LOG_DEBUG, "%s\n", current_iter->c_str());
	if (__strstr((char*)current_iter->c_str(), "batteryManager=") != NULL) {
		CD_log::print(CD_log::LOG_DEBUG, "ohoh\n");
	}
	*/
	D_element *p_element = NULL;
	if( __str_start_with((char*)current_iter->c_str(), (char*)"<?") ){
		//<?와 ?>를 잘라서 순수 데이터만 던짐
   		char *left_replace = NULL;
   		unsigned int left_replace_size = 0;
   		__str_replace((char*)current_iter->c_str(), (char*)"<?", (char*)"", &left_replace, &left_replace_size);
		char *right_replace = NULL;
		unsigned int right_replace_size = 0;
		__str_replace(left_replace, (char*)"?>", (char*)"", &right_replace, &right_replace_size);
   		free(left_replace);

		p_element = new D_element;

		p_element->e_tag_type = D_element::DECLARATION;
		if( __parssing_one_line(right_replace, p_element) ){
			free(right_replace);
			delete p_element;
			return NULL;
		}
		p_element->p_parent = p_parent;

		free(right_replace);
        _element_list.push_back(p_element);
		return p_element;
	}
	else if( __str_start_with((char*)current_iter->c_str(), (char*)"<!--") ){
		//<!--와 -->를 잘라서 순수 데이터만 던짐
   		char *left_replace = NULL;
   		unsigned int left_replace_size = 0;
   		__str_replace((char*)current_iter->c_str(), (char*)"<!--", (char*)"", &left_replace, &left_replace_size);
		char *right_replace = NULL;
		unsigned int right_replace_size = 0;
		__str_replace(left_replace, (char*)"-->", (char*)"", &right_replace, &right_replace_size);
   		free(left_replace);

		p_element = new D_element;

		p_element->e_tag_type = D_element::COMMENT;
		p_element->data = right_replace;
		p_element->p_parent = p_parent;

		free(right_replace);
        _element_list.push_back(p_element);
		return p_element;
	}
	else if( __str_start_with((char*)current_iter->c_str(), (char*)"</") ){
		return NULL;
	}
	else if( __str_start_with((char*)current_iter->c_str(), (char*)"<") ){
		if( __str_end_with((char*)current_iter->c_str(), (char*)"/>") ){
   			char *left_replace = NULL;
   			unsigned int left_replace_size = 0;
   			__str_replace((char*)current_iter->c_str(), (char*)"<", (char*)"", &left_replace, &left_replace_size);
			char *right_replace = NULL;
			unsigned int right_replace_size = 0;
			__str_replace(left_replace, (char*)"/>", (char*)"", &right_replace, &right_replace_size);
   			free(left_replace);

			p_element = new D_element;

			p_element->e_tag_type = D_element::DEFINITION_NO_GROUP_TAG;
			if( __parssing_one_line(right_replace, p_element) ){
				free(right_replace);
				delete p_element;
				return NULL;
			}
			p_element->p_parent = p_parent;

			free(right_replace);
            _element_list.push_back(p_element);
			return p_element;
		}
		else if( __str_end_with((char*)current_iter->c_str(), (char*)">") ){
   			char *left_replace = NULL;
   			unsigned int left_replace_size = 0;
   			__str_replace((char*)current_iter->c_str(), (char*)"<", (char*)"", &left_replace, &left_replace_size);
			char *right_replace = NULL;
			unsigned int right_replace_size = 0;
			__str_replace(left_replace, (char*)">", (char*)"", &right_replace, &right_replace_size);
   			free(left_replace);
			
			p_element = new D_element;

			p_element->e_tag_type = D_element::DEFINITION_GROUP_TAG;
			if( __parssing_one_line(right_replace, p_element) ){
				free(right_replace);
				delete p_element;
				return NULL;
			}
			p_element->p_parent = p_parent;

			free(right_replace);
			//----Grup_tag는 특수하게 하위가 있다는 뜻이기 때문에 하위로 전달
			current_iter++;
			for(; current_iter != data_list.end(); current_iter++){
				D_element *p_child_element = __parssing(data_list, current_iter, p_element);
				if (p_child_element != NULL) {
					p_element->childs.push_back( p_child_element );
				}
				else {
					break;
				}
			}
			p_element->p_parent = p_parent;
            _element_list.push_back(p_element);
			return p_element;
		}
	}
	else{	//data
		p_parent->data = *current_iter;
		return NULL;
	}
	return NULL;
}

unsigned int CD_xml::__parssing_one_line(char* one_line, D_element *p_element){
	if(p_element == NULL){
		CD_log::print(CD_log::LOG_ERROR, "ERROR !!! CD_xml::__parssing_one_line() NULL element !!\n");

	}
	if(__strstr(one_line, "=") == NULL && __strstr(one_line, "\"") == NULL){
		//data니까 =이 없는거여, attribute가 없으니 "도 없는거구
		//data에 넣어주면 되겠다
		p_element->data = one_line;
	}
	char **p_line_split = NULL;
	unsigned int line_split_count = 0;
	__str_split(one_line, (char*)" ", &p_line_split, &line_split_count);
	
	for(unsigned int split_count = 0; split_count < line_split_count; split_count++){
		if(split_count == 0){
			p_element->name = p_line_split[split_count];
			// printf("name[%s]\n", element.name.c_str());
			continue;
		}

		//value에 값이 하.. 시벙... space 값이 들어 있을 경우엔 어떻게 처리할지에 대해 생각해야함....
		std::string str_line = p_line_split[split_count];
		unsigned int daum_count = __str_find_count((char*)str_line.c_str(), (char*)"\"");
		while( (daum_count % 2) && (split_count + 1 < line_split_count) ){
			str_line = str_line + " " + p_line_split[++split_count];
			daum_count = __str_find_count((char*)str_line.c_str(), (char*)"\"");
		}

		char **p_key_value = NULL;
		unsigned int key_value_count = 0;
		__str_split((char*)str_line.c_str(), (char*)"=", &p_key_value, &key_value_count);
		//내용에 "=" 문자가 많을 경우를 생각하여 = 는 가장 앞에 있는 것만 취급하도록 함
		std::string str_key = "";
		std::string str_value = "";
		if(key_value_count != 2){
			if (key_value_count > 2) {
				str_key = p_key_value[0];
				for (unsigned int value_count = 1; value_count < key_value_count; value_count++) {
					str_value = str_value + "=" + p_key_value[value_count];
				}
			}
			else {	//여기에 올 경우 name도 아니면서 2개가 안될 경우 여기로 던짐
				CD_log::print(CD_log::LOG_ERROR, "ERROR !!! CD_xml::__parssing_one_line() attribute Count Error !!\n");
				for(unsigned int key_value_count_increase = 0; key_value_count_increase < key_value_count; key_value_count_increase++){
					CD_log::print(CD_log::LOG_ERROR, "ERROR !!! %s !!\n", p_key_value[key_value_count_increase]);
				}
				__str_split_delete(&p_key_value, key_value_count);
				__str_split_delete(&p_line_split, line_split_count);
				return -1;
			}
		}
		else {
			str_key = p_key_value[0];
			str_value = p_key_value[1];
		}
		char * daum_delete = NULL;
		unsigned int daum_delete_size = 0;
		if(__strstr((char*)str_value.c_str(), "\"")){
			__str_replace((char*)str_value.c_str(), (char*)"\"", (char*)"", &daum_delete, &daum_delete_size);
		}
		else{
			__str_replace((char*)str_value.c_str(), (char*)"\'", (char*)"", &daum_delete, &daum_delete_size);
		}
		// printf("KEY[%s], value[%s]\n", p_key_value[0], daum_delete);
		p_element->attribute.push_back( std::make_pair(str_key, std::string(daum_delete)) );

		__str_split_delete(&p_key_value, key_value_count);
	}

	__str_split_delete(&p_line_split, line_split_count);
	return 0;
}

#endif /*  _IN_CD_XML */