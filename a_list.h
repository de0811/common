#pragma once
#include <iostream>
#include <new>
/***
 * 2011년 4월 어느 날
 * 
 * 초기 부분밖에 남지 않아 다시 재 작업 진행
 * 추가 개발 필요 사항 : merge, sort 기능 필요
 * - vector에 엮을 interface 구조 정립
 * - pair 필요
*/

template <typename T> class A_list {
private:
	unsigned int _size;
	struct _NODE {
		T _data;
		_NODE *_nxt_point = NULL;
		_NODE *_prv_point = NULL;
	};
	_NODE *_begin;  //맨 앞의 값을 따라다님 초기엔 __end
	_NODE __end;    //_end의 prv_point를 사용하기 위해서 데이터 공간이 필요함
	_NODE *_end;    //end는 항상 NULL이어야함

public:
	class Iterator {
		_NODE* _current;
	public:
		Iterator() { _current = NULL; }
		Iterator(_NODE* node) { _current = node; }
		_NODE* get_node() { return _current; }
		Iterator operator ++(int) {	//iter++
			_current = _current->_nxt_point;
			return _current;
		}
		Iterator operator --(int) {	//iter--
			_current = _current->_prv_point;
			return _current;
		}
		Iterator operator ++() {	//++iter
			_current = _current->_nxt_point;
			return _current;
		}
		Iterator operator --() {	//--iter
			_current = _current->_prv_point;
			return _current;
		}
		T* operator ->() {
			return &_current->_data;
		}
		bool operator ==(Iterator iter) {
			return this->_current == iter._current;
		}
		bool operator !=(Iterator iter) {
			return this->_current != iter._current;
		}
		T& operator*() const
		{	// return designated value
			return _current->_data;
		}
	};
	void clear();
	~A_list() { clear(); }
	A_list() : _begin(&__end), _end(&__end), _size(0)
	{
	}
	Iterator back() { return Iterator(_end->_prv_point); }
	Iterator end() { return Iterator(_end); }
	Iterator begin() { return Iterator(_begin); }
	bool empty() { return _begin == _end; }
	unsigned int size() { return _size; }

private:
	_NODE* __insert(T data, _NODE *pre, _NODE *nxt);
public:
	Iterator insert(Iterator iter, T data);

	void push_back(T data);
	void push_front(T data);
	void remove(Iterator iter);
};

//모든 Node insert를 대응
template <typename T>
typename A_list<T>::_NODE* A_list<T>::__insert(T data, _NODE *prv, _NODE *nxt) {
	_NODE* newbi = new _NODE();
	newbi->_data = data;
	if (prv == _end && nxt == _end && prv == _begin) { //_begin 과 _end가 같을 경우
		_begin = newbi;
		prv = newbi;
		prv->_nxt_point = nxt;
		nxt->_prv_point = prv;
	}
	else if (prv == NULL && nxt == _end && nxt->_prv_point == NULL) { //최초일 경우 prv == NULL nxt가 NULL이고 
		_begin = newbi;
		newbi->_nxt_point = nxt;
		nxt->_prv_point = _begin;
	}
	else if (prv == NULL && _begin == nxt) {	//_begin 앞에 넣어야할 때 push_front
		_begin->_prv_point = newbi;
		newbi->_nxt_point = _begin;
		_begin = newbi;
		nxt->_prv_point = newbi;
	}
	else {   //뒤에 무조건 begin에 값이 있다면
		if (prv == NULL || nxt == NULL) {
			delete newbi;
			return NULL;
		}
		if (prv->_nxt_point != nxt || nxt->_prv_point != prv) {
			delete newbi;
			return NULL;
		}
		prv->_nxt_point = newbi;
		newbi->_prv_point = prv;
		nxt->_prv_point = newbi;
		newbi->_nxt_point = nxt;
	}
	++_size;
	return newbi;
}
template <typename T>
typename A_list<T>::Iterator A_list<T>::insert(Iterator iter, T data) {
	return Iterator( __insert(data, iter.get_node()->_prv_point, iter.get_node()) );
}

template <typename T>
void A_list<T>::push_back(T data) {
	__insert(data, _end->_prv_point, _end);
}

template <typename T>
void A_list<T>::push_front(T data) {
	__insert(data, NULL, _begin);
}

template <typename T>
void A_list<T>::remove(Iterator iter) {
	//insert 바꿧으니 remove도 변경해야함
	_NODE *_current = iter.get_node();
	if (_current == NULL || \
		_current == _end) return;

	if (_current == _begin) { //내가 최초라면?
		_begin = _begin->_nxt_point;
		_begin->_prv_point = NULL;
		delete _current;
		--_size;
		return;
	}

	_NODE * back = _current->_prv_point;
	_NODE * front = _current->_nxt_point;
	back->_nxt_point = front;
	front->_prv_point = back;
	--_size;
	delete _current;
}

template <typename T>
void A_list<T>::clear() {
	while (_begin != _end) {
		remove(Iterator(_begin));
	}
}