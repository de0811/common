﻿


#include <stdio.h>
#include <stdarg.h>
#include <string>

#ifndef _IN_LOG_MANAGER
#define _IN_LOG_MANAGER

class CD_log{
public:
    enum{
        LOG_ALL,
        LOG_INFO,
        LOG_DEBUG,
        LOG_WARING,
        LOG_ERROR,
    };
	static const int MAX_BUFF = 10000;
    static void print(unsigned int type, char const* const _Format, ...);
    static void print(unsigned int type, int deeps, char const* const _Format, ...);
	static void print(unsigned int type, int deeps, char buf[MAX_BUFF]);
};
void CD_log::print(unsigned int type, char const* const _Format, ...){

    va_list _ArgList;
    char buf[MAX_BUFF] = {0,};

    va_start(_ArgList, _Format);
    vsprintf(buf, _Format, _ArgList);
    va_end(_ArgList);

	print(type, 0, buf);
}

void CD_log::print(unsigned int type, int deeps, char buf[MAX_BUFF]) {
    // if(type == LOG_DEBUG) return;
    std::string str_log = "";
    switch(type){
        case LOG_INFO:
        str_log += "";
        break;
        case LOG_DEBUG:
        str_log += "DDDD ";
        break;
        case LOG_WARING:
        str_log += "@@@@ ";
        break;
        case LOG_ERROR:
        str_log += "!!!! ";
        break;
    }
    for(int i = 0 ; i < deeps; i++) str_log += "\t";

    str_log += buf;

    printf(str_log.c_str());
}

void CD_log::print(unsigned int type, int deeps, char const* const _Format, ...){
    va_list _ArgList;
    char buf[MAX_BUFF] = {0,};

    va_start(_ArgList, _Format);
    vsprintf(buf, _Format, _ArgList);
    va_end(_ArgList);
	print(type, deeps, buf);
}
#endif /* _IN_LOG_MANAGER*/